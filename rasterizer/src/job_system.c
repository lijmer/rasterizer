#include "job_system.h"
#include "tiny_windows.h"
#include <Windows.h>

#include <stdlib.h>
#include <stdio.h>

//Microsoft code :S
//https://msdn.microsoft.com/en-us/library/xcb2z8hs.aspx
//
// Usage: SetThreadName ((DWORD)-1, "MainThread");
//
const DWORD MS_VC_EXCEPTION = 0x406D1388;
#pragma pack(push,8)
typedef struct tagTHREADNAME_INFO
{
	DWORD dwType; // Must be 0x1000.
	LPCSTR szName; // Pointer to name (in user addr space).
	DWORD dwThreadID; // Thread ID (-1=caller thread).
	DWORD dwFlags; // Reserved for future use, must be zero.
} THREADNAME_INFO;
#pragma pack(pop)
void SetThreadName(DWORD dwThreadID, const char* threadName) {
	THREADNAME_INFO info;
	info.dwType = 0x1000;
	info.szName = threadName;
	info.dwThreadID = dwThreadID;
	info.dwFlags = 0;
#pragma warning(push)
#pragma warning(disable: 6320 6322)
	__try {
		RaiseException(MS_VC_EXCEPTION, 0, sizeof(info) / sizeof(ULONG_PTR), (ULONG_PTR*)&info);
	}
	__except (EXCEPTION_EXECUTE_HANDLER) {
	}
#pragma warning(pop)
}



#define CRASH() (*(volatile int*)(0) = 0x1337);

typedef struct JobQueue
{
	Job* jobs;
	size_t capacity;
	size_t front;
	size_t end;
	size_t stored;
	CRITICAL_SECTION lock;
}JobQueue;

void initQueue(JobQueue* queue)
{
	queue->capacity = 65536;
	queue->jobs = malloc(queue->capacity * sizeof(Job));
	queue->front = 0;
	queue->end = 0;
	queue->stored = 0;
	InitializeCriticalSection(&queue->lock);
}
void cleanUpQueue(JobQueue* queue)
{
	free(queue->jobs);
}
void pushQueue(JobQueue* queue, const Job* job)
{
	if (queue->stored >= queue->capacity)
		CRASH();

	queue->jobs[queue->end++] = *job;

	queue->stored++;

	if (queue->end == queue->capacity)
		queue->end = 0;
}
Job popQueue(JobQueue* queue)
{
	if (queue->stored == 0)
		CRASH();

	Job result = queue->jobs[queue->front++];
	queue->stored--;


	if (queue->front == queue->capacity)
		queue->front = 0;

	return result;
}

typedef struct ThreadData
{
	JobSystem* jobmanager;
	int threadid;
}ThreadData;

typedef struct JobSystem
{
	HANDLE* threads;
	HANDLE* waitevents;
	HANDLE* stopevents;
	Job* jobs;
	ThreadData* threaddata;
	CRITICAL_SECTION critsection;
	int threadcount;
	JobQueue queue;
}JobSystem;


static BOOL askForNewJob(JobSystem* jm, Job *job)
{
	EnterCriticalSection(&jm->critsection);
	BOOL result = FALSE;
	if (jm->queue.stored > 0u)
	{
		result = TRUE;
		*job = popQueue(&jm->queue);
	}
	LeaveCriticalSection(&jm->critsection);
	return result;
}

static DWORD WINAPI threadFunc(void* data)
{
	ThreadData* td = (ThreadData*)data;
	HANDLE* waitevent = &td->jobmanager->waitevents[td->threadid];
	HANDLE* stopevent = &td->jobmanager->stopevents[td->threadid];
	Job* job = &td->jobmanager->jobs[td->threadid];
	for (;;)
	{
		//wait until this thread is enabled
		WaitForSingleObject(*waitevent, INFINITE);

		//check if thread is supposed to stop running
		if (WaitForSingleObject(*stopevent, 0) == WAIT_OBJECT_0)
			return 0;

		//if the thread is not supposed to quit, it means there is a job waiting 

		for (;;)
		{
			//execute the job we got
			job->function(job->param);

			//ask the job manager for a new job
			BOOL gotnewjob = askForNewJob(td->jobmanager, job);

			//if the thread did not get a new job, the thread stops executing jobs
			if (!gotnewjob)
				break;
		}

		//indicate that the thread is done executing jobs
		ResetEvent(*waitevent);
	}
}

JobSystem* JS_allocJobSystem()
{
	return (JobSystem*)malloc(sizeof(JobSystem));
}
void JS_freeJobSystem(JobSystem* jm)
{
	free(jm);
}


int32_t JS_initJobSystem(JobSystem* jm, int threadcount)
{
	if (jm == NULL) return 1;

	jm->threads = malloc(sizeof(HANDLE) * threadcount);
	jm->threadcount = threadcount;

	jm->waitevents = malloc(sizeof(HANDLE) * threadcount);
	jm->stopevents = malloc(sizeof(HANDLE) * threadcount);

	jm->threaddata = malloc(sizeof(ThreadData) * threadcount);
	jm->jobs = malloc(sizeof(Job) * threadcount);

	initQueue(&jm->queue);

	InitializeCriticalSection(&jm->critsection);

	for (int i = 0; i < threadcount; ++i)
	{
		jm->waitevents[i] = CreateEventA(0, TRUE, FALSE, NULL);
		jm->stopevents[i] = CreateEventA(0, TRUE, FALSE, NULL);

		jm->threaddata[i].jobmanager = jm;
		jm->threaddata[i].threadid = i;

		//create a new thread
		jm->threads[i] = CreateThread(
			NULL,               //thread security attributes (NULL for none)
			0,                  //stack size in bytes (0 -> default stack size)
			&threadFunc,        //function to be executed on new thread
			jm->threaddata + i, //parameter to be passed to the function
			0,                  //creation flags (0 for none)
			NULL);              //thread ID as output (pass NULL to ignore output)


		char name[256];
		sprintf_s(name, sizeof(name), "Rasterizer worker thread %d", i);
		DWORD threadID = GetThreadId(jm->threads[i]);
		SetThreadName(threadID, name);
	}

	return 0;
}

void JS_queueJob(JobSystem* js, const Job* job)
{
	pushQueue(&js->queue, job);
}

void JS_start(JobSystem* jm)
{
	int threads_to_launch = min(jm->threadcount, (int)jm->queue.stored);

	for (int i = 0; i < threads_to_launch; ++i)
		jm->jobs[i] = popQueue(&jm->queue);

	for (int i = 0; i < threads_to_launch; ++i)
		SetEvent(jm->waitevents[i]);
}

void JS_sync(JobSystem* jm)
{
	for (int i = 0; i < jm->threadcount; ++i)
	{
		for (;;)
		{
			if (WaitForSingleObject(jm->waitevents[i], 0) == WAIT_TIMEOUT)
			{
				break;
			}
			if (SwitchToThread() == 0)
				Sleep(1);
		}
	}
}

uint32_t JS_getSystemCoreCount()
{
	SYSTEM_INFO info;
	GetSystemInfo(&info);
	return (uint32_t)info.dwNumberOfProcessors;
}
