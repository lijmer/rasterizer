//All STB implementation files go in here, just to make things easier
#define STB_IMAGE_IMPLEMENTATION 1
#define STB_IMAGE_WRITE_IMPLEMENTATION 1

#pragma warning(push)
#pragma warning(disable: 4244) //conversion from x to y, possible loss of data
#define _CRT_SECURE_NO_WARNINGS

#include "stb_image.h"
#include "stb_image_write.h"

#pragma warning(pop)
