#ifndef INCLUDED_JOB_MANAGER_H
#define INCLUDED_JOB_MANAGER_H

#include <stdint.h>

typedef struct JobSystem JobSystem;
typedef struct Job
{
	void(*function)(void*);
	void* param;
}Job;


JobSystem* JS_allocJobSystem();
void JS_freeJobSystem(JobSystem*);

int32_t JS_initJobSystem(JobSystem*, int threadcount);
void JS_queueJob(JobSystem*, const Job* job);
void JS_start(JobSystem*);
void JS_sync(JobSystem*);

uint32_t JS_getSystemCoreCount();

#endif//INCLUdED_JOB_MANAGER_H
