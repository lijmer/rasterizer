#include "util.h"

#include <assert.h>
#include <stdlib.h>

#include "tiny_windows.h"
#include <Windows.h>
#include <SDL.h>

//include libraries
#include "linmath.h"
#include "stb_image.h"
#include "stb_image_write.h"

#include "rasterizer.h"
#include "import.h"
#include "palettize.h"

#include <immintrin.h>

#define SCRWIDTH 1920
#define SCRHEIGHT 1080
#define CAM_NEAR 0.4f
#define CAM_FAR 100.0f

#define PI 3.1415926538f

int getAVXSupportFlag();

void saveRenderTarget(const RenderTarget* rt, const char* filepath)
{
	const size_t size = rt->width * rt->height;
	uint32_t *buffer = MALLOC64(size * sizeof(uint32_t));
	detileRenderTarget(rt, buffer, rt->width, rt->height, rt->width);
	for (size_t i = 0; i < size; ++i) buffer[i] |= 0xff000000;

	stbi_write_png(
		filepath,
		rt->width,
		rt->height,
		4,
		buffer,
		0);

	FREE64(buffer);
}
void saveDepthBuffer(const DepthBuffer* db, const char* filepath)
{
	uint32_t* tmp_buffer = malloc(db->width * db->height * sizeof(uint32_t));
	for (uint32_t y = 0; y < db->height; ++y)
	{
		for (uint32_t x = 0; x < db->width; ++x)
		{
			float d = db->buffer[y * db->pitch + x];

			const float n = CAM_NEAR;
			const float f = CAM_FAR;

			const float c1 = f / n;
			const float c0 = 1.0f - c1;

			d = 1.0f / (c0 * d + c1);

			//d = CAM_NEAR * CAM_FAR / (CAM_FAR + CAM_NEAR - d * (CAM_FAR - CAM_NEAR));

			uint32_t di = (uint32_t)(d * 255.0f);
			const uint32_t color =
				(di << 16) | (di << 8) | di | 0xff000000;


			tmp_buffer[y * db->width + x] = color;
		}
	}

	stbi_write_png(
		filepath,
		db->width,
		db->height,
		4,
		tmp_buffer,
		0);
}

void upscaleBuffer(
	const uint32_t* buffer_in, uint32_t width_in, uint32_t height_in,
	uint32_t* buffer_out, uint32_t width_out, uint32_t height_out)
{
	if (width_in == width_out && height_in == height_out)
	{
		memcpy(buffer_out, buffer_in, height_out * width_out * sizeof(uint32_t));
	}
	else
	{
		if (width_out % width_in == 0 && height_out % height_in == 0)
		{
			//more effecient upscaling path
			const int dx = width_out / width_in;
			const int dy = height_out / height_in;
			uint32_t* scanline_out = buffer_out;
			for (uint32_t y = 0; y < height_out; ++y)
			{
				const int ty = y / dy;
				const uint32_t* scanline_in = buffer_in + ty * width_in;
				for (uint32_t x = 0; x < width_out; ++x)
				{
					const int tx = x / dx;
					scanline_out[x] = scanline_in[tx];
				}
				scanline_out += width_out;
			}
		}
		else
		{
			//slow upscaling path
			for (uint32_t y = 0; y < height_out; ++y)
			{
				const int ty = (int)(((float)y / (float)height_out) * height_in);
				for (uint32_t x = 0; x < width_out; ++x)
				{
					const int tx = (int)(((float)x / (float)width_out) * width_in);
					buffer_out[y * width_out + x] = buffer_in[ty * width_in + tx];
				}
			}
		}
	}
}

int checkIsVisible(float3 min, float3 max, float4x4 wvp)
{
	const float3 bb[8] = {
		{ min[0], min[1], min[2] },
		{ min[0], min[1], max[2] },
		{ min[0], max[1], min[2] },
		{ min[0], max[1], max[2] },
		{ max[0], min[1], min[2] },
		{ max[0], min[1], max[2] },
		{ max[0], max[1], min[2] },
		{ max[0], max[1], max[2] }
	};
	
	int mask = 0x3f;
	for (int i = 0; i < 8; ++i)
	{
		__declspec(align(16)) float4 v = { bb[i][0], bb[i][1], bb[i][2], 1.0f };
		mat4x4_mul_vec4(v, wvp, v);
		mask &= genClipMask(_mm_load_ps(v));
	}

	return mask == 0;
}

#define CAPTURE() do{ \
if(capturing)\
{\
	char path[MAX_PATH];\
	sprintf_s(path, MAX_PATH, "capture/c%u.png", capture_frame_count);\
	saveRenderTarget(&srt, path);\
	sprintf_s(path, MAX_PATH, "capture/d%u.png", capture_frame_count);\
	saveDepthBuffer(&db, path);\
	capture_frame_count++;\
}}while(0)

#undef main
int main(int argc, char** argv)
{
	//{
	//	int cpui[4];
	//	__cpuid(cpui, 0);
	//	int n_ids = cpui[0];
	//
	//	for (int i = 0; i <= n_ids; ++i)
	//	{
	//		__cpuidex(cpui, i, 0);
	//
	//	}
	//}
	if (!getAVXSupportFlag())
	{
		PRINTF("Too bad buddy, your CPU does not have AVX support :(\n");
		return 1;
	}


	int frame_number = 0;
	int obj_idx = 0;
	//int err_count = 0;
	//int broken_frame_count = 0;

	((void)argc);
	((void)argv);
	SDL_Window* window = SDL_CreateWindow("title", 100, 100, SCRWIDTH, SCRHEIGHT, 0);
	SDL_Renderer* renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
	SDL_Texture* frame_buffer = SDL_CreateTexture(
		renderer,
		SDL_PIXELFORMAT_ARGB8888,
		SDL_TEXTUREACCESS_STREAMING,
		SCRWIDTH,
		SCRHEIGHT);



	const uint32_t width = SCRWIDTH;
	const uint32_t height = SCRHEIGHT;
	RenderTarget srt;
	createRenderTarget(width, height, &srt);

	DepthBuffer db;
	createDepthBuffer(width, height, &db);

	uint32_t* screenbuffer = MALLOC64(SCRWIDTH * SCRHEIGHT * sizeof(uint32_t));
	RenderTarget rt = {
		.buffer = screenbuffer,
		.width = SCRWIDTH,
		.height = SCRHEIGHT,
		.pitch = SCRWIDTH,
	};

	//load an error texture
	Texture texture_;
	importTexture("../assets/test_texture.png", &texture_);

#if 0
	Texture test_mip_tex;
	{
		test_mip_tex.width = 128;
		test_mip_tex.height = 128;
		test_mip_tex.widthf = 128;
		test_mip_tex.heightf = 128;
		test_mip_tex.mip_count = 8;
		test_mip_tex.palette = malloc(256 * sizeof(uint32_t));
		test_mip_tex.palette[0] = 0xff0000;
		test_mip_tex.palette[1] = 0x00ff00;
		test_mip_tex.palette[2] = 0x0000ff;
		test_mip_tex.palette[3] = 0x00ffff;
		test_mip_tex.palette[4] = 0xff00ff;
		test_mip_tex.palette[5] = 0xffff00;
		test_mip_tex.palette[6] = 0xffffff;
		test_mip_tex.palette[7] = 0x000000;
	}
#endif

	LARGE_INTEGER lastTime, freq;
	QueryPerformanceCounter(&lastTime);
	QueryPerformanceFrequency(&freq);

	initRasterizer();

	Model model;
	int32_t result = importModel("../assets/sponza.obj", &model);
	if (result != 0) return result;

	uint32_t key[SDL_NUM_SCANCODES] = { 0 };
	uint32_t key_down[SDL_NUM_SCANCODES];
	uint32_t key_up[SDL_NUM_SCANCODES];

	vec3 camera_position = { 0.0f, 1.8f, 0.0f };
	float camera_rot_x = 0.0f, camera_rot_y = PI * .5f;

	//vec3 camera_position = { -1.0f, 1.0f, 4.0f };
	//float camera_rot_x = -.1f * PI, camera_rot_y = .1f * PI;

	//vec3 camera_position = { 0, 0, 2 };
	//float camera_rot_x = 0, camera_rot_y = 0;


	float t = 0;
	int mx = 0, my = 0;
	int running = 1;
	while (running)
	{
		memset(key_down, 0, sizeof(key_down));
		memset(key_up, 0, sizeof(key_up));
		SDL_Event event;
		while (SDL_PollEvent(&event))
		{
			switch (event.type)
			{
			case SDL_QUIT:
				running = 0;
				break;
			case SDL_KEYDOWN:
				if (event.key.keysym.scancode == SDL_SCANCODE_ESCAPE)
					running = 0;
				if (event.key.keysym.scancode == SDL_SCANCODE_P)
					saveRenderTarget(&srt, "screenshot.png");
				key[event.key.keysym.scancode] = 1;
				key_down[event.key.keysym.scancode] = 1;
				break;
			case SDL_KEYUP:
				key[event.key.keysym.scancode] = 0;
				key_up[event.key.keysym.scancode] = 1;
				break;
			case SDL_MOUSEMOTION:
				mx = event.motion.x;
				my = event.motion.y;
				break;
			}
		}

		float avg_clear_time;
		{
			LARGE_INTEGER start, end;
			QueryPerformanceCounter(&start);

			//clearRenderTarget(&srt, 0);
			clearDepthBuffer(&db, 1.0f);

			QueryPerformanceCounter(&end);

			float t0 = (float)(end.QuadPart - start.QuadPart) / (float)freq.QuadPart;

			static float tot;
			static int it;
			it++;
			tot += t0;

			avg_clear_time = tot / (float)it;
		}

		LARGE_INTEGER currentTime;
		QueryPerformanceCounter(&currentTime);
		float dt =
			(float)(currentTime.QuadPart - lastTime.QuadPart) /
			(float)freq.QuadPart;
		lastTime = currentTime;
		{
			float speed = 5.0f;
			if (key[SDL_SCANCODE_LCTRL]) speed *= 2.5f;
			if (key[SDL_SCANCODE_LSHIFT]) speed /= 10.0f;
			vec4 movement = { 0 };
			if (key[SDL_SCANCODE_W]) movement[2] -= speed * dt;
			if (key[SDL_SCANCODE_S]) movement[2] += speed * dt;
			if (key[SDL_SCANCODE_A]) movement[0] -= speed * dt;
			if (key[SDL_SCANCODE_D]) movement[0] += speed * dt;
			if (key[SDL_SCANCODE_Q]) movement[1] -= speed * dt;
			if (key[SDL_SCANCODE_E]) movement[1] += speed * dt;

			mat4x4 rotation;
			mat4x4_identity(rotation);
			mat4x4_rotate_Y(rotation, rotation, camera_rot_y);
			mat4x4_rotate_X(rotation, rotation, camera_rot_x);

			mat4x4_mul_vec4(movement, rotation, movement);

			if (key[SDL_SCANCODE_UP]) camera_rot_x -= dt    * (speed / 2.5f);
			if (key[SDL_SCANCODE_DOWN])camera_rot_x += dt   * (speed / 2.5f);
			if (key[SDL_SCANCODE_LEFT])  camera_rot_y -= dt * (speed / 2.5f);
			if (key[SDL_SCANCODE_RIGHT]) camera_rot_y += dt * (speed / 2.5f);

			vec3_add(camera_position, camera_position, movement);
		}

		if (key_down[SDL_SCANCODE_MINUS])
			obj_idx = max(0, (obj_idx - 1));
		if (key_down[SDL_SCANCODE_EQUALS])
			obj_idx++;

		t += 0.005f;

		int capturing = 0;
		if (key_down[SDL_SCANCODE_C]) capturing = 1;

		uint32_t capture_frame_count = 0;

		CAPTURE();

		//Calculate view projection matrix
		mat4x4 view_proj_matrix;
		{
			//calculate matrix
			mat4x4 view_matrix;
			{
				mat4x4 rotation;
				mat4x4_identity(rotation);
				mat4x4_rotate_Y(rotation, rotation, camera_rot_y);
				mat4x4_rotate_X(rotation, rotation, camera_rot_x);
				mat4x4_transpose(rotation, rotation);

				mat4x4 translation;
				mat4x4_translate(
					translation,
					-camera_position[0],
					-camera_position[1],
					-camera_position[2]);

				mat4x4_mul(view_matrix, rotation, translation);
			}

			//calculate perspective matrix
			mat4x4 perspective_matrix;
			{
				const float ratio = (float)srt.width / (float)srt.height;
				mat4x4_perspective(perspective_matrix, 1.0f, ratio, CAM_NEAR, CAM_FAR);
			}


			mat4x4_mul(view_proj_matrix, perspective_matrix, view_matrix);
		}

		LARGE_INTEGER start, end;
		QueryPerformanceCounter(&start);
		int culled = 0;

		setRenderTarget(&srt, &db);

		{
			Object* current_obj = &model.objects[0];
			float4x4 parent_matrix;
			mat4x4_identity(parent_matrix);
			struct { Object* obj; float4x4 parent_matrix; } stack[4096];
			int stackidx = 0;

			for (;;)
			{
				float4x4 world_matrix;
				mat4x4_mul(world_matrix, parent_matrix, current_obj->world_matrix);

				//float4x4 rot;
				//mat4x4_identity(rot);
				//mat4x4_rotate_X(rot, rot, t);
				//mat4x4_rotate_Y(rot, rot, t);
				//mat4x4_rotate_Z(rot, rot, PI * .5f);
				//mat4x4_mul(world_matrix, world_matrix, rot);

				for (uint32_t i = 0; i < current_obj->mesh_count; ++i)
				{
					Mesh* mesh = current_obj->meshes[i];
					Texture* texture = NULL;
					Material* material = current_obj->materials[i];
					if (material != NULL)
						texture = material->texture;

					if (texture == NULL)
						texture = &texture_;

					mat4x4 wvp;
					mat4x4_mul(wvp, view_proj_matrix, world_matrix);

					int isVisible = checkIsVisible(mesh->min, mesh->max, wvp);

					if (isVisible != 0)
					{
						//draw
						drawMesh(
							mesh->vertex_buffer,
							mesh->texcoord_buffer,
							mesh->vertex_count,
							mesh->index_buffer,
							mesh->index_count,
							texture,
							wvp);

						CAPTURE();
					}
					else
						culled++;
				}
				if (current_obj->mesh_count == 0 && current_obj->child_count == 0)
					OutputDebugStringA("empty node!\n");

				const uint32_t child_count = current_obj->child_count;
				if (child_count > 0)
				{
					for (uint32_t i = 1; i < child_count; ++i)
					{
						stack[stackidx].obj = current_obj->children + i;
						mat4x4_dup(stack[stackidx].parent_matrix, world_matrix);
						++stackidx;
					}
					current_obj = current_obj->children + 0;
					mat4x4_dup(parent_matrix, world_matrix);
				}
				else
				{
					if (stackidx == 0) break;

					--stackidx;
					current_obj = stack[stackidx].obj;
					mat4x4_dup(parent_matrix, stack[stackidx].parent_matrix);
				}

			}
		}

		flushRenderer();

		QueryPerformanceCounter(&end);
		float t0 = (float)(end.QuadPart - start.QuadPart) / (float)(freq.QuadPart);

		static float tot0 = 0.0f;
		static int it0 = 0;

		it0++;
		tot0 += t0;

		float avg_draw_time = tot0 / (float)it0;


		float avg_detile_time;
		{
			QueryPerformanceCounter(&start);
			detileRenderTarget(&srt, rt.buffer, rt.width, rt.height, rt.pitch);
			QueryPerformanceCounter(&end);
			float t1 = (float)(end.QuadPart - start.QuadPart) / (float)(freq.QuadPart);

			static float tot1 = 0.0f;
			static int it1 = 0;

			it1++;
			tot1 += t1;
			avg_detile_time = tot1 / (float)it1;
		}


		print(&rt, 5, 8, 0xff0000, "delta time: %f ms", dt * 1000.0f);
		print(&rt, 5, 16, 0xff0000, "avg clear  time: %f ms", avg_clear_time * 1000.0f);
		print(&rt, 5, 24, 0xff0000, "avg render time: %f ms", avg_draw_time * 1000.0f);
		print(&rt, 5, 32, 0xff0000, "avg detile time: %f ms", avg_detile_time * 1000.0f);
		print(&rt, 5, 40, 0xff0000, "mx: %d my: %d", mx, my);
		print(&rt, 5, 48, 0xff0000, "%d meshes culled", culled);

		//swap buffers
		SDL_UpdateTexture(frame_buffer, NULL, rt.buffer, SCRWIDTH * 4);
		SDL_RenderCopy(renderer, frame_buffer, NULL, NULL);
		SDL_RenderPresent(renderer);

		frame_number++;
	}


	freeTexture(&texture_);
	freeModel(&model);
	shutdownRasterizer();
	FREE64(srt.buffer);
	FREE64(rt.buffer);
	FREE64(db.buffer);

	SDL_DestroyTexture(frame_buffer);
	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);

#ifdef _DEBUG
	_CrtDumpMemoryLeaks();
#endif

	return 0;
}


int CALLBACK WinMain(
	HINSTANCE hinstance,
	HINSTANCE unused0,
	LPSTR unused1,
	int unused2)
{
	((void)hinstance);
	((void)unused0);
	((void)unused1);
	((void)unused2);
	return main(__argc, __argv);
}
