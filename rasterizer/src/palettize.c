#include "util.h"
#include "palettize.h"

#include <immintrin.h>

#define PRECISION 6
#define NULL_ID (~0u)
#define STACK_TRAVERSAL_SIZE 4096

typedef struct Node
{
	uint32_t refs;           //counts how many times the colour was added
	uint32_t r, g, b;        //total colour values
	uint32_t child[8];       //indices to the children
	uint32_t child_count;    //number of children
	uint32_t parent;         //index to the parent node
	uint32_t palette_idx;    //index of the colour in the palette
}Node;

typedef struct Octree
{
	Node* node_pool;
	uint32_t node_pool_size;
	uint32_t node_pool_capacity;

	uint32_t* nodes_per_level[PRECISION];
	uint32_t nodes_per_level_size[PRECISION];
	uint32_t nodes_per_level_capacity[PRECISION];

}Octree;

#define PIXEL_R(p) (((p) >> 16) & 0xff)
#define PIXEL_G(p) (((p) >>  8) & 0xff)
#define PIXEL_B(p) ( (p)        & 0xff)
#define INIT_NODE(n, p) do { \
  (n) = (Node){0}; \
  for(int q = 0; q < 8; ++q) (n).child[q] = NULL_ID; \
	(n).parent = p; \
  } while(0) \

#if 0
//calculates the traversal direction based on color and level
static inline uint32_t getIdx(__m128i rgb, int level)
{
	__m128i a = rgb;
	a = _mm_and_si128(a, _mm_set1_epi32(0x80 >> level));
	a = _mm_sll_epi32(a, _mm_set_epi32(0, 0, 0, level));
	int mask = _mm_movemask_epi8(a);
	return _pext_u32(mask, 0x1110);
}
#endif

static uint32_t getIdx(uint32_t r, uint32_t g, uint32_t b, int level)
{
	__m128i a = _mm_set_epi32(r, g, b, 0);
	a = _mm_and_si128(a, _mm_set1_epi32(0x80 >> level));
	a = _mm_sll_epi32(a, _mm_setr_epi32(level, 0, 0, 0));
	int mask = _mm_movemask_epi8(a);
	return _pext_u32(mask, 0x1110);
	//const uint32_t shift = 7 - level;
	//return
	//	(((r & (0x80 >> level)) << 2) |
	//	(((g & (0x80 >> level)) << 1) |
	//	(((b & (0x80 >> level)) << 0);
}

static uint32_t constructPixel(uint32_t r, uint32_t g, uint32_t b, uint32_t c)
{
	return ((r / c) << 16) | ((g / c) << 8) | (b / c);
}

static uint32_t getLeafCount(Octree* octree)
{
	uint32_t leaf_count = 0;

	Node* node = octree->node_pool; //set the node to the root

	uint32_t stack[STACK_TRAVERSAL_SIZE];
	uint32_t stackidx = 0;

	for (;;)
	{
		if (node->child_count < 8)
			leaf_count++;

		if (node->child_count > 0)
		{
			//Set the next node to traverse to, to the first child
			Node* next_node = NULL;
			int i;
			for (i = 0; i < 8; ++i)
			{
				const uint32_t child_idx = node->child[i];
				if (child_idx != NULL_ID)
				{
					next_node = &octree->node_pool[child_idx];
					break;
				}
			}

			//push all the other children on the stack
			for (i = i + 1; i < 8; ++i)
			{
				const uint32_t child_idx = node->child[i];
				if (child_idx != NULL_ID)
					stack[stackidx++] = child_idx;
			}

			node = next_node;
		}
		else
		{
			if (stackidx == 0)
			{
				break;
			}

			//pop an item of the stack
			const uint32_t next_idx = stack[--stackidx];
			node = octree->node_pool + next_idx;
		}
	}

	return leaf_count;
}


uint32_t getPaletteIdx(const Octree* tree, uint32_t pixel)
{
	const Node* node = tree->node_pool;

	const uint32_t r = PIXEL_R(pixel);
	const uint32_t g = PIXEL_G(pixel);
	const uint32_t b = PIXEL_B(pixel);

	for (int l = 0; l < PRECISION; ++l)
	{
		const uint32_t idx = getIdx(r, g, b, l);
		if (node->child[idx] == NULL_ID)
			break;

		node = tree->node_pool + node->child[idx];
	}

	return node->palette_idx;
}


//returns the index the new child node
static uint32_t createNode(
	Octree* tree,
	int32_t level,
	uint32_t parent_id,
	int32_t child_idx)
{
	//If we ran out of memory in our pool, we double the capacity
	if (tree->node_pool_size >= tree->node_pool_capacity)
	{
		tree->node_pool = REALLOC64(
			tree->node_pool,
			tree->node_pool_capacity * 2 * sizeof(Node));

		ASSERT(tree->node_pool != NULL);

		tree->node_pool_capacity = tree->node_pool_capacity * 2;
	}


	Node* parent = &tree->node_pool[parent_id];

	//Create a new child
	const uint32_t child_id = tree->node_pool_size++;
	Node* child = &tree->node_pool[child_id];
	INIT_NODE(*child, parent_id);


	parent->child[child_idx] = child_id;
	parent->child_count++;

	//child level
	const int cl = level;
	ASSERT(tree->nodes_per_level_size[cl] <= tree->nodes_per_level_capacity[cl]);
	if (tree->nodes_per_level_size[cl] == tree->nodes_per_level_capacity[cl])
	{
		if (tree->nodes_per_level_capacity[cl] == 0)
		{
			tree->nodes_per_level_capacity[cl] = 8;
			tree->nodes_per_level[cl] = MALLOC64(8 * sizeof(uint32_t));
		}
		else
		{
			tree->nodes_per_level_capacity[cl] *= 2;
			tree->nodes_per_level[cl] = REALLOC64(
				tree->nodes_per_level[cl],
				tree->nodes_per_level_capacity[cl] * sizeof(uint32_t));
		}
	}

	const uint32_t a = tree->nodes_per_level_size[cl]++;
	tree->nodes_per_level[cl][a] = (uint32_t)(child - tree->node_pool);

	return child_id;
}

static void freeOctree(Octree* tree)
{
	FREE64(tree->node_pool);
	for (int i = 0; i < PRECISION; ++i)
		FREE64(tree->nodes_per_level[i]);

	*tree = (Octree) { 0 };
}

int32_t palettize256(
	uint32_t* buffer_in,
	size_t pixel_count,
	uint8_t* buffer_out,
	uint32_t* palette)
{

	//init octree
	Octree octree = {
	.node_pool = MALLOC64(128 * sizeof(Node)),
	.node_pool_capacity = 128,
	.node_pool_size = 1
	};
	INIT_NODE(octree.node_pool[0], NULL_ID);


	//build octree
	for (size_t i = 0; i < pixel_count; ++i)
	{
		const uint32_t pixel = buffer_in[i] & 0xffffff;
		uint32_t node_idx = 0;


		const uint32_t r = PIXEL_R(pixel);
		const uint32_t g = PIXEL_G(pixel);
		const uint32_t b = PIXEL_B(pixel);


		Node* node = &octree.node_pool[node_idx];
		node->r += r;
		node->g += g;
		node->b += b;
		node->refs++;

		for (int l = 0; l < PRECISION - 1; ++l)
		{
			const uint32_t child_idx = getIdx(r, g, b, l);
			if (node->child[child_idx] == NULL_ID)
			{
				createNode(&octree, l, node_idx, child_idx);

				//Create node might have moved the buffer, which invalidated all our
				// pointers to nodes, so we need to reassign it
				node = &octree.node_pool[node_idx];
			}

			node_idx = node->child[child_idx];
			node = &octree.node_pool[node_idx];
			node->r += r;
			node->g += g;
			node->b += b;
			node->refs++;
		}
	}

	//reduce
	{
		const uint32_t target_leaf_count = 256;
		uint32_t leaf_count = getLeafCount(&octree);

		//Reduce until we hit our colour target
		while (leaf_count > target_leaf_count)
		{
			uint32_t array_idx = ~0u;
			for (int32_t i = PRECISION - 1; i >= 0; --i)
			{
				if (octree.nodes_per_level_size[i] > 0)
				{
					array_idx = i;
					break;
				}
			}
			ASSERT(array_idx != ~0u);


			uint32_t* const array = octree.nodes_per_level[array_idx];
			uint32_t* array_size = &octree.nodes_per_level_size[array_idx];

			//Find the least significant node
			const uint32_t end = *array_size;
			uint32_t least_sig_idx = end - 1;
			for (int32_t i = end - 2; i >= 0; --i)
			{
				const Node* left = &octree.node_pool[array[i]];
				const Node* right = &octree.node_pool[array[least_sig_idx]];
				if (left->refs < right->refs)
					least_sig_idx = i;
			}
			uint32_t least_sig_node_idx = array[least_sig_idx];

			//get a pointer to the node and the parent
			Node* node = &octree.node_pool[least_sig_node_idx];
			Node* parent = &octree.node_pool[node->parent];



			//if the parent already was a leaf, we have 1 less leaf in our octree,
			// otherwise we are turning our parent into a leaf, so the number of leafs
			// stays equal
			if (parent->child_count < 8)
				--leaf_count;


			int valid = 0;

			//Remove the leaf from the parent
			for (int32_t c = 0; c < 8; ++c)
			{
				if (parent->child[c] == least_sig_node_idx)
				{
					valid = 1;
					parent->child[c] = NULL_ID;
					parent->child_count--;
					break;
				}
			}
			ASSERT(valid != 0);

			//Remove the deleted node from the array of nodes per level
			array[least_sig_idx] = array[end - 1];
			*array_size = (*array_size) - 1;
		}
	}

	//construct palette
	{
		Node* stack[STACK_TRAVERSAL_SIZE];
		int32_t stackidx = 0;

		Node* node = octree.node_pool;

		uint32_t palette_idx = 0;

		for (;;)
		{
			if (node->child_count < 8)
			{
				node->palette_idx = palette_idx;
				palette[palette_idx++] = constructPixel(
					node->r, node->g, node->b, node->refs);
			}

			if (node->child_count > 0)
			{
				Node* c0 = NULL;
				int i;
				for (i = 0; i < 8; ++i)
				{
					if (node->child[i] != NULL_ID)
					{
						c0 = octree.node_pool + node->child[i];
						break;
					}
				}
				for (i = i + 1; i < 8; ++i)
				{
					if (node->child[i] != NULL_ID)
						stack[stackidx++] = octree.node_pool + node->child[i];
				}

				node = c0;
			}
			else
			{
				if (stackidx == 0)
					break;

				node = stack[--stackidx];
			}
		}
	}

	//remap colors using the palette
	for (size_t i = 0; i < pixel_count; ++i)
	{
		buffer_out[i] = (uint8_t)getPaletteIdx(&octree, buffer_in[i]);
	}

	freeOctree(&octree);
	return 0;
}
