#ifndef INCLUDED_RASTERIZER_H
#define INCLUDED_RASTERIZER_H

#include <stdint.h>
#include "linmath.h"

typedef struct RenderTarget
{
	uint32_t* buffer;
	uint32_t width;
	uint32_t height;
	uint32_t pitch;
	uint32_t padding;
}RenderTarget; //16 bytes

typedef struct DepthBuffer
{
	float* buffer;
	uint32_t width;
	uint32_t height;
	uint32_t pitch;
	uint32_t padding;
}DepthBuffer;

typedef struct Texture
{
	uint32_t* buffer;

	uint8_t* mip_ptrs[12];
	uint8_t* idx_buffer;
	uint8_t* morton;
	uint32_t* palette;

	int32_t max_mip;
	uint32_t pitch;

	uint32_t width;
	uint32_t height;

	float widthf;
	float heightf;

	uint32_t shift_w;
	uint32_t shift_h;
}Texture;

void initRasterizer();
void shutdownRasterizer();

int32_t createRenderTarget(
	uint32_t width,
	uint32_t height,
	RenderTarget* out_render_target);
int32_t createDepthBuffer(
	uint32_t width,
	uint32_t height,
	DepthBuffer* out_depth_buffer);

int32_t detileRenderTarget(
	const RenderTarget* in,
	uint32_t* out_buffer,
	uint32_t out_width,
	uint32_t out_height,
	uint32_t out_pitch);


void setRenderTarget(
	RenderTarget* render_target,
	DepthBuffer* depth_buffer);

void clearRenderTarget(RenderTarget*, uint32_t);
void clearDepthBuffer(DepthBuffer*, float);

void print(
	RenderTarget *rt,
	int x,
	int y,
	uint32_t color,
	const char* str,
	...);


void drawMesh(
	const float3* verts,
	const float2* texcoords,
	uint32_t vertcount,
	const uint32_t* indices,
	uint32_t indexcount,
	const Texture* texture,
	float4x4 matrix);

void flushRenderer();

#endif//INCLUDED_RASTERIZER_H
