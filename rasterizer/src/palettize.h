#pragma once

int32_t palettize256(
	uint32_t* buffer_in,
	size_t pixel_count,
	uint8_t* buffer_out,
	uint32_t* palette);
