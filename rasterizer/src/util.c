#include "util.h"

#include "tiny_windows.h"
#include <Windows.h>

#include <stdio.h>

void PRINTF(const char* str, ...)
{
	char buffer[65536];

	va_list argptr;
	va_start(argptr, str);
	vsprintf_s(buffer, sizeof(buffer), str, argptr);
	va_end(argptr);

	OutputDebugStringA(buffer);
}
