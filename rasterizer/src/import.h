#pragma once

#include "rasterizer.h"


typedef struct Mesh
{
	uint32_t vertex_count;
	uint32_t index_count;

	float3* vertex_buffer;

	float2* texcoord_buffer;

	uint32_t* index_buffer;

	//AABB
	float3 min;
	float3 max;
}Mesh;

typedef struct Material
{
	char name[1024];
	Texture* texture;
}Material;

typedef struct Object Object;
struct Object
{
	Mesh** meshes;
	Material** materials;
	uint32_t mesh_count;

	Object* children;
	uint32_t child_count;
	uint32_t padding;
	float4x4 world_matrix;
};

typedef struct Model
{
	uint32_t mesh_count;
	uint32_t material_count;
	uint32_t texture_count;
	uint32_t object_count;

	Mesh* meshes;
	Material* materials;
	Texture* textures;
	Object* objects;
} Model;

/*
return values:
* -1 -> failed to load from file
* -2 -> texture is not power of two
* -3 -> failed to allocate buffer
* -4 -> parameter was NULL
*/
int32_t importTexture(const char* filepath, Texture* texture);
void freeTexture(Texture* texture);

int32_t importModel(
	const char* filepath,
	Model* out_model);
void freeModel(Model* model);
