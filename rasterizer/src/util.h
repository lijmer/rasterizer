#pragma once

#ifdef _DEBUG
# define _CRTDBG_MAP_ALLOC
# include <crtdbg.h>
# include <stdlib.h>
# define MALLOC64(x) _aligned_malloc_dbg(x, 64, __FILE__, __LINE__)
# define REALLOC64(p, x) _aligned_realloc_dbg(p, x, 64, __FILE__, __LINE__)
# define FREE64(x) _aligned_free_dbg(x)
#else
# include <stdlib.h>
# define MALLOC64(x) _aligned_malloc(x, 64)
# define REALLOC64(p, x) _aligned_realloc(p, x, 64)
# define FREE64(x) _aligned_free(x)
#endif

#include <stdint.h>
#include <assert.h>
#include <immintrin.h>

#define ASSERT(x) assert(x)
#define MORTON32(x) _pdep_u32(x, 0x55555555u)
#define MORTON64(x) _pdep_u64(x, 0x55555555ull)

#define restrict __restrict

#ifdef min
#undef min
#endif

#ifdef max
#undef max
#endif

#define min(x, y) ((x) < (y) ? (x) : (y))
#define max(x, y) ((x) > (y) ? (x) : (y))
#define min3(x, y, z)    min(min(x, y), z)
#define max3(x, y, z)    max(max(x, y), z)
#define min4(x, y, z, w) min(min(x, y), min(z, w))
#define max4(x, y, z, w) max(max(x, y), max(z, w))

enum
{
	SUCCES = 0,
	ERR_ALLOC_FAILURE = 1,
	ERR_PARAM_WAS_NULL = 2,
};

inline int32_t isPowerOfTwo(int32_t x)
{
	//http://www.exploringbinary.com/ten-ways-to-check-if-an-integer-is-a-power-of-two-in-c/
	return ((x != 0) && !(x & (x - 1)));
}

void PRINTF(const char* str, ...);
