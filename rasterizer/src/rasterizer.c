#include "util.h"
#include <stdint.h>
#include <assert.h>
#include <stdio.h>

#include "tiny_windows.h"
#include <Windows.h>

#include <immintrin.h> //AVX

#include "linmath.h"

#include "rasterizer.h"
#include "job_system.h"

#define PI 3.1415926538f

#define WINDING_ORDER_CW    0
#define WINDING_ORDER_CCW   1
#define WINDING_ORDER WINDING_ORDER_CCW

#define BACKFACE_CULLING 1

#define ENABLE_MULTI_THREADING 1
#define ENABLE_MIPMAPPING 1

#define TILE_SHIFT 5
#define TILE_SIZE (1 << TILE_SHIFT)
#define TILE_MASK (TILE_SIZE - 1)

#define ATTR_COUNT 2
#define MAX_VERT 9

#define MAX_JOB_PER_TILE 65536

#define NO_BILINEAR 0b1
#define DEBUG_SETTINGS NO_BILINEAR

typedef struct Triangle
{
	int32_t vx[3];
	int32_t vy[3];
	float vertsz[3];
	float vertsw[3];
	float attr[ATTR_COUNT * 3];
	const Texture* texture;
}Triangle;

typedef struct Tile
{
	uint32_t x;
	uint32_t y;

	uint32_t* cbuffer;
	float* dbuffer;

	volatile LONG tri_count;
	Triangle tri_buffer[MAX_JOB_PER_TILE];
}Tile;

static char charset[54][5][5];
static int charlut[255];

static JobSystem* job_system;

static RenderTarget* s_render_target;
static DepthBuffer* s_depth_buffer;

static Tile* s_tiles;
static uint32_t s_tile_count;
static uint32_t s_tile_pitch;

static void setchar(
	int idx,
	const char* c0,
	const char* c1,
	const char* c2,
	const char* c3,
	const char* c4)
{
	memcpy(charset[idx][0], c0, 5);
	memcpy(charset[idx][1], c1, 5);
	memcpy(charset[idx][2], c2, 5);
	memcpy(charset[idx][3], c3, 5);
	memcpy(charset[idx][4], c4, 5);
}

static void initCharSet()
{
	setchar(0, "  #  ", " # # ", "#   #", "#####", "#   #"); //a
	setchar(1, "#### ", "#   #", "#### ", "#   #", "#### "); //b
	setchar(2, " ####", "#    ", "#    ", "#    ", " ####"); //c
	setchar(3, "#### ", "#   #", "#   #", "#   #", "#### "); //d
	setchar(4, "#####", "#    ", "#####", "#    ", "#####"); //e
	setchar(5, "#####", "#    ", "###  ", "#    ", "#    "); //f
	setchar(6, " ####", "#    ", "#  ##", "#   #", " ### "); //g
	setchar(7, "#   #", "#   #", "#####", "#   #", "#   #"); //h
	setchar(8, " ### ", "  #  ", "  #  ", "  #  ", " ### "); //i
	setchar(9, "  ###", "   # ", "   # ", "#  # ", " ##  "); //j
	setchar(10, "#  # ", "# #  ", "##   ", "# #  ", "#  # "); //k
	setchar(11, "#    ", "#    ", "#    ", "#    ", "#### "); //l
	setchar(12, "#   #", "## ##", "# # #", "#   #", "#   #"); //m
	setchar(13, "#   #", "##  #", "# # #", "#  ##", "#   #"); //n
	setchar(14, " ### ", "#   #", "#   #", "#   #", " ### "); //o
	setchar(15, "#### ", "#   #", "#### ", "#    ", "#    "); //p
	setchar(16, " ### ", "#   #", "# # #", "#  # ", " ## #"); //q
	setchar(17, "#### ", "#   #", "#### ", "#  # ", "#   #"); //r
	setchar(18, "#####", "#    ", "#####", "    #", "#####"); //s
	setchar(19, "#####", "  #  ", "  #  ", "  #  ", "  #  "); //t
	setchar(20, "#   #", "#   #", "#   #", "#   #", "#####"); //u
	setchar(21, "#   #", "#   #", "#   #", " # # ", "  #  "); //v
	setchar(22, "#   #", "#   #", "# # #", "# # #", "#####"); //w
	setchar(23, "#   #", " # # ", "  #  ", " # # ", "#   #"); //x
	setchar(24, "#   #", " # # ", "  #  ", "  #  ", "  #  "); //y
	setchar(25, "#####", "   # ", "  #  ", " #   ", "#####"); //z
	setchar(26, " ### ", "##  #", "# # #", "#  ##", " ### "); //0
	setchar(27, "  #  ", "###  ", "  #  ", "  #  ", "#####"); //1
	setchar(28, " ### ", "#   #", "  ## ", " #   ", "#####"); //2
	setchar(29, "#### ", "    #", "  ## ", "    #", "#### "); //3
	setchar(30, "  ## ", " # # ", "#  # ", "#####", "   # "); //4
	setchar(31, "#### ", "#    ", "#### ", "    #", "#### "); //5
	setchar(32, " ### ", "#    ", "#### ", "#   #", " ### "); //6
	setchar(33, "#####", "    #", "   # ", "  #  ", "  #  "); //7
	setchar(34, " ### ", "#   #", " ### ", "#   #", " ### "); //8
	setchar(35, " ### ", "#   #", " ####", "    #", " ### "); //9
	setchar(36, "     ", "     ", "     ", "     ", "  #  "); //.
	setchar(37, "     ", "     ", "     ", "  #  ", " #   "); //,
	setchar(38, "     ", "     ", " ### ", "     ", "     "); //-
	setchar(39, "  #  ", "  #  ", "#####", "  #  ", "  #  "); //+
	setchar(40, "     ", "  #  ", "     ", "  #  ", "     "); //:
	setchar(41, "     ", "  #  ", "     ", "  #  ", " #   "); //;
	setchar(53, "#####", "#####", "#####", "#####", "#####"); //;
	//setchar(0, "", "", "", "", "");

	char chars[] = "abcdefghijklmnopqrstuvwxyz0123456789.,-+:;";
	for (int i = 0; i < 255; ++i)
	{
		int idx = 53;
		for (int j = 0; j < 53; ++j)
		{
			if (chars[j] == (char)i)
			{
				idx = j;
				break;
			}
		}
		charlut[i] = idx;
	}
}

void initRasterizer()
{
	initCharSet();
	job_system = JS_allocJobSystem();
	JS_initJobSystem(job_system, JS_getSystemCoreCount());
}
void shutdownRasterizer()
{
	FREE64(s_tiles);
	s_tiles = NULL;
	JS_freeJobSystem(job_system);
}

int32_t createRenderTarget(
	uint32_t width,
	uint32_t height,
	RenderTarget* out_render_target)
{
	uint32_t actual_width = (width + TILE_MASK) & ~TILE_MASK;
	uint32_t actual_height = (height + TILE_MASK) & ~TILE_MASK;
	uint32_t number_of_pixels = actual_width * actual_height;

	RenderTarget *rt = out_render_target;
	rt->width = width;
	rt->height = height;
	rt->buffer = MALLOC64(number_of_pixels * sizeof(uint32_t));
	rt->pitch = actual_width;
	return 0;
}

int32_t createDepthBuffer(
	uint32_t width,
	uint32_t height,
	DepthBuffer* out_depth_buffer)
{
	uint32_t actual_width = (width + TILE_MASK) & ~TILE_MASK;
	uint32_t actual_height = (height + TILE_MASK) & ~TILE_MASK;
	uint32_t number_of_pixels = actual_width * actual_height;

	DepthBuffer *db = out_depth_buffer;
	db->width = width;
	db->height = height;
	db->buffer = MALLOC64(number_of_pixels * sizeof(float));
	db->pitch = actual_width;
	return 0;
}

int32_t detileRenderTarget(
	const RenderTarget* in,
	uint32_t* out_buffer,
	uint32_t out_width,
	uint32_t out_height,
	uint32_t out_pitch)
{
	if (in->width > out_width || in->height > out_height)
		return 1;

	const uint32_t in_actual_width = (in->width + TILE_MASK) & ~TILE_MASK;
	const uint32_t in_actual_height = (in->height + TILE_MASK) & ~TILE_MASK;

	uint32_t* tile_buffer = in->buffer;
	uint32_t* out_scanline = out_buffer;
	for (uint32_t ty = 0; ty < in_actual_height; ty += TILE_SIZE)
	{
		for (uint32_t tx = 0; tx < in_actual_width; tx += TILE_SIZE)
		{
			//Copy the tile
			uint32_t* tile_scanline = tile_buffer;
			uint32_t* out_scanline0 = out_scanline + tx;
			for (uint32_t y = 0; y < TILE_SIZE && ty + y < out_height; y += 2)
			{
				for (uint32_t x = 0; x < TILE_SIZE; x += 4)
				{
					const __m256i a = _mm256_permutevar8x32_epi32(
						*(__m256i*)&tile_scanline[x * 2],
						_mm256_set_epi32(7, 6, 3, 2, 5, 4, 1, 0));
					__m128i* const top = (__m128i*)&out_scanline0[x];
					__m128i* const bot = (__m128i*)&out_scanline0[out_pitch + x];
					*top = _mm256_extracti128_si256(a, 0);
					*bot = _mm256_extracti128_si256(a, 1);
				}

				tile_scanline += TILE_SIZE * 2;
				out_scanline0 += out_pitch * 2;
			}

			tile_buffer += TILE_SIZE * TILE_SIZE;
		}
		out_scanline += out_pitch * TILE_SIZE;
	}

	return 0;
}


void setRenderTarget(
	RenderTarget* render_target,
	DepthBuffer* depth_buffer)
{
	flushRenderer();

	if (render_target == s_render_target &&
		depth_buffer == s_depth_buffer)
		return;

	ASSERT(render_target->width == depth_buffer->width);
	ASSERT(render_target->height == depth_buffer->height);

	s_render_target = render_target;
	s_depth_buffer = depth_buffer;

	const uint32_t width = render_target->width;
	const uint32_t height = render_target->height;

	const uint32_t tile_count_x = (width + TILE_MASK) / TILE_SIZE;
	const uint32_t tile_count_y = (height + TILE_MASK) / TILE_SIZE;
	s_tile_pitch = tile_count_x;

	s_tile_count = tile_count_x * tile_count_y;
	if (s_tiles == NULL)
	{
		s_tiles = MALLOC64(s_tile_count * sizeof(Tile));
	}
	else
	{
		s_tiles = REALLOC64(s_tiles, s_tile_count * sizeof(Tile));
	}


	uint32_t* cbuffer = render_target->buffer;
	float* dbuffer = depth_buffer->buffer;
	for (uint32_t y = 0, ty = 0; ty < tile_count_y; y += TILE_SIZE, ++ty)
	{
		for (uint32_t x = 0, tx = 0; tx < tile_count_x; x += TILE_SIZE, ++tx)
		{
			Tile* tile = &s_tiles[tx + ty * s_tile_pitch];
			tile->x = x;
			tile->y = y;
			tile->cbuffer = cbuffer;
			tile->dbuffer = dbuffer;
			tile->tri_count = 0;

			cbuffer += TILE_SIZE * TILE_SIZE;
			dbuffer += TILE_SIZE * TILE_SIZE;
		}
	}
}

void clearRenderTarget(RenderTarget* rendertarget, uint32_t color)
{
	const uint32_t actual_width = (rendertarget->width + TILE_MASK) & ~TILE_MASK;
	const uint32_t actual_height = (rendertarget->height + TILE_MASK) & ~TILE_MASK;

	const size_t size = actual_width * actual_height;
	uint32_t* buffer = rendertarget->buffer;
	if (color == 0)
	{
		//faster routine if we are clearing to black
		memset(buffer, 0, size * sizeof(uint32_t));
	}
	else
	{
		const size_t size_8 = size & ~7;
		size_t i = 0;
		for (; i < size_8; i += 8)
			buffer[i + 0] = buffer[i + 1] = buffer[i + 2] = buffer[i + 3] =
			buffer[i + 4] = buffer[i + 5] = buffer[i + 6] = buffer[i + 7] = color;
		for (; i < size; ++i) buffer[i] = color;
	}
}
void clearDepthBuffer(DepthBuffer* depth_buffer, float depth)
{
	const uint32_t actual_width = (depth_buffer->width + TILE_MASK) & ~TILE_MASK;
	const uint32_t actual_height = (depth_buffer->height + TILE_MASK) & ~TILE_MASK;

	const size_t size = actual_width * actual_height;
	if (depth == 0)
	{
		//faster routine if we are clearing to 0
		memset(depth_buffer->buffer, 0, size * sizeof(float));
	}
	else
	{
		//We can use this optimized clear routine since we can assume that a
		// depth buffer is at least 32 byte aligned, and that it is always going to
		// be a multiple TILE_SIZE * TILE_SIZE bytes long.
		const size_t size_8 = size;
		__m256* db = (__m256*)depth_buffer->buffer;
		__m256 val = _mm256_set1_ps(depth);
		size_t i = 0;
		for (; i < size_8; i += 8, db++)
			*db = val;
	}
}

void print(
	RenderTarget *rt,
	int x,
	int y,
	uint32_t color,
	const char* str,
	...)
{
	char buffer[65536];
	va_list argptr;
	va_start(argptr, str);
	vsprintf_s(buffer, sizeof(buffer), str, argptr);
	va_end(argptr);

	uint32_t* pbuffer = rt->buffer + x + y * rt->pitch - 6;

	size_t len = strlen(buffer);
	for (size_t i = 0; i < len; ++i)
	{
		pbuffer += 6;
		char c = buffer[i];
		if (c == ' ') continue;
		if (c >= 'A' && c <= 'Z') c = c - 'A' + 'a';
		int idx = charlut[c];
		uint32_t* scanline = pbuffer;
		for (int y0 = 0; y0 < 5; ++y0)
		{
			uint32_t* p = scanline;
			for (int x0 = 0; x0 < 5; ++x0, ++p)
			{
				if (charset[idx][y0][x0] == '#')
				{
					*p = color;
					*(p + rt->pitch + 1) = 0x0;
				}
			}
			scanline += rt->pitch;
		}
	}
}

#define SHIFT 4
#define MUL (1 << SHIFT)
#define MULF ((float)MUL)
#define MASK (MUL - 1)

uint32_t addblend(uint32_t c0, uint32_t c1)
{
	uint32_t r = ((c0 >> 16) & 0xff) + ((c1 >> 16) & 0xff);
	uint32_t g = ((c0 >> 8) & 0xff) + ((c1 >> 8) & 0xff);
	uint32_t b = ((c0 >> 0) & 0xff) + ((c1 >> 0) & 0xff);
	r = min(r, 0xff);
	g = min(g, 0xff);
	b = min(b, 0xff);
	return (r << 16) | (g << 8) | b;
}

static inline int genClipMask(__m128 v)
{
	//this checks a vertex against the 6 planes, and stores if they are inside
	// or outside of the plane

	//w contains the w component of the vector in all 4 slots
	const __m128 w = _mm_shuffle_ps(v, v, _MM_SHUFFLE(3, 3, 3, 3));

	//subtract the vector from w, and store in a
	const __m128 a = _mm_sub_ps(w, v);
	//add the vector to w, and store in b
	const __m128 b = _mm_add_ps(w, v);

	//compare if a and b are less than zero,
	// and store the result in fmaska, and fmaskk
	const __m128 fmaska = _mm_cmplt_ps(a, _mm_setzero_ps());
	const __m128 fmaskb = _mm_cmplt_ps(b, _mm_setzero_ps());

	//convert those masks to integers, and spread the bits using pdep
	const int maska = _pdep_u32(_mm_movemask_ps(fmaska), 0x55);
	const int maskb = _pdep_u32(_mm_movemask_ps(fmaskb), 0xAA);

	//or the masks together and and the together with all bits set to 1
	// NOTE only the bits 0x3f are actually used
	return (maska | maskb) & 0x3f;
}


static void rasterizeTriangle(
	const Triangle tri,
	uint32_t tile_x,
	uint32_t tile_y,
	uint32_t* restrict cbuffer,
	float* restrict dbuffer)
{
	const int32_t* restrict vx = tri.vx;
	const int32_t* restrict vy = tri.vy;
	const float* restrict vw = tri.vertsw;
	const float* restrict vz = tri.vertsz;
	const float* restrict attr = tri.attr;
	const Texture* restrict texture = tri.texture;

	//Calculate the triangle bounding box
	const uint32_t minx = (min3(vx[0], vx[1], vx[2]) + MASK) >> SHIFT;
	const uint32_t miny = (min3(vy[0], vy[1], vy[2]) + MASK) >> SHIFT;
	const uint32_t maxx = (max3(vx[0], vx[1], vx[2]) + MASK) >> SHIFT;
	const uint32_t maxy = (max3(vy[0], vy[1], vy[2]) + MASK) >> SHIFT;


#define Q 1
	//Adjust the bounding box for the block size
	const uint32_t minxi = max(minx & ~0x3, tile_x);//& ~(Q - 1);
	const uint32_t minyi = max(miny & ~0x1, tile_y);//& ~(Q - 1);
	const uint32_t maxxi = min(maxx, tile_x + TILE_SIZE);
	const uint32_t maxyi = min(maxy, tile_y + TILE_SIZE);

	//calculate the starting point for the scanline
	const int ox = (minxi - tile_x) << 1;
	const int oy = (minyi - tile_y) >> 1;
	uint32_t* restrict scanline = cbuffer + ox + oy * TILE_SIZE * 2;
	float* restrict dscanline = dbuffer + ox + oy * TILE_SIZE * 2;


	const int32_t dx01 = vx[0] - vx[1], dy01 = vy[0] - vy[1];
	const int32_t dx12 = vx[1] - vx[2], dy12 = vy[1] - vy[2];
	const int32_t dx20 = vx[2] - vx[0], dy20 = vy[2] - vy[0];

	const int32_t fdx01 = dx01 << SHIFT, fdy01 = dy01 << SHIFT;
	const int32_t fdx12 = dx12 << SHIFT, fdy12 = dy12 << SHIFT;
	const int32_t fdx20 = dx20 << SHIFT, fdy20 = dy20 << SHIFT;

	const __m256i fdx01_v = _mm256_set1_epi32(fdx01);
	const __m256i fdx12_v = _mm256_set1_epi32(fdx12);
	const __m256i fdx20_v = _mm256_set1_epi32(fdx20);

	const __m256i fdx01_v2 = _mm256_set1_epi32(fdx01 * 2);
	const __m256i fdx12_v2 = _mm256_set1_epi32(fdx12 * 2);
	const __m256i fdx20_v2 = _mm256_set1_epi32(fdx20 * 2);


	const __m256i fdy01_v = _mm256_set1_epi32(fdy01);
	const __m256i fdy12_v = _mm256_set1_epi32(fdy12);
	const __m256i fdy20_v = _mm256_set1_epi32(fdy20);

	const __m256i fdy01_v8 = _mm256_set1_epi32(fdy01 * 4);
	const __m256i fdy12_v8 = _mm256_set1_epi32(fdy12 * 4);
	const __m256i fdy20_v8 = _mm256_set1_epi32(fdy20 * 4);

	int32_t c0 = dy01 * vx[0] - dx01 * vy[0];
	int32_t c1 = dy12 * vx[1] - dx12 * vy[1];
	int32_t c2 = dy20 * vx[2] - dx20 * vy[2];

	if (!(dy01 < 0 || (dy01 == 0 && dx01 > 0))) c0--;
	if (!(dy12 < 0 || (dy12 == 0 && dx12 > 0))) c1--;
	if (!(dy20 < 0 || (dy20 == 0 && dx20 > 0))) c2--;

	const int32_t area =
		(vx[2] - vx[0]) * (vy[1] - vy[0]) -
		(vy[2] - vy[0]) * (vx[1] - vx[0]);
	const __m256 r_areaf_v = _mm256_set1_ps(1.0f / (float)area);

	const int32_t w0_row = c0 + dx01 * (minyi << SHIFT) - dy01 * (minxi << SHIFT);
	const int32_t w1_row = c1 + dx12 * (minyi << SHIFT) - dy12 * (minxi << SHIFT);
	const int32_t w2_row = c2 + dx20 * (minyi << SHIFT) - dy20 * (minxi << SHIFT);

#if 1
	const __m256i offsetX = _mm256_set_epi32(3, 2, 3, 2, 1, 0, 1, 0);
	const __m256i offsetX0 = _mm256_mullo_epi32(offsetX, fdy01_v);
	const __m256i offsetX1 = _mm256_mullo_epi32(offsetX, fdy12_v);
	const __m256i offsetX2 = _mm256_mullo_epi32(offsetX, fdy20_v);

	const __m256i offsetY = _mm256_set_epi32(1, 1, 0, 0, 1, 1, 0, 0);
	const __m256i offsetY0 = _mm256_mullo_epi32(offsetY, fdx01_v);
	const __m256i offsetY1 = _mm256_mullo_epi32(offsetY, fdx12_v);
	const __m256i offsetY2 = _mm256_mullo_epi32(offsetY, fdx20_v);


	__m256i w0_row_v = _mm256_sub_epi32(_mm256_add_epi32(_mm256_set1_epi32(w0_row), offsetY0), offsetX0);
	__m256i w1_row_v = _mm256_sub_epi32(_mm256_add_epi32(_mm256_set1_epi32(w1_row), offsetY1), offsetX1);
	__m256i w2_row_v = _mm256_sub_epi32(_mm256_add_epi32(_mm256_set1_epi32(w2_row), offsetY2), offsetX2);

	const __m256 vz0 = _mm256_set1_ps(vz[0]);
	const __m256 vz10 = _mm256_set1_ps(vz[1] - vz[0]);
	const __m256 vz20 = _mm256_set1_ps(vz[2] - vz[0]);
	const __m256 vw0 = _mm256_set1_ps(vw[0]);
	const __m256 vw10 = _mm256_set1_ps(vw[1] - vw[0]);
	const __m256 vw20 = _mm256_set1_ps(vw[2] - vw[0]);

	__m256 a0[ATTR_COUNT], a10[ATTR_COUNT], a20[ATTR_COUNT];
	const float* at = attr;
	for (int a = 0; a < ATTR_COUNT; ++a)
	{
		const float at0 = *(at++);
		const float at1 = *(at++);
		const float at2 = *(at++);

		a0[a] = _mm256_set1_ps(at0);
		a10[a] = _mm256_set1_ps(at1 - at0);
		a20[a] = _mm256_set1_ps(at2 - at0);
	}

	for (uint32_t y = minyi; y < maxyi; y += 2)
	{
		__m256i w0 = w0_row_v;
		__m256i w1 = w1_row_v;
		__m256i w2 = w2_row_v;

		__m256i* restrict  p = (__m256i*)scanline;
		__m256*  restrict dp = (__m256*)dscanline;
		for (uint32_t x = minxi; x < maxxi; x += 4, ++p, ++dp)
		{
			const __m256i w_all = _mm256_or_si256(w0, _mm256_or_si256(w1, w2));
			const __m256i zero = _mm256_setzero_si256();
			const __m256i mask0_8 = _mm256_or_si256(
				_mm256_cmpeq_epi32(w_all, zero),
				_mm256_cmpgt_epi32(w_all, zero));

			if (_mm256_movemask_epi8(mask0_8) != 0)
			{
				//b = w2 * r_areaf;
				//c = w0 * r_areaf;
				const __m256 b = _mm256_mul_ps(_mm256_cvtepi32_ps(w2), r_areaf_v);
				const __m256 c = _mm256_mul_ps(_mm256_cvtepi32_ps(w0), r_areaf_v);

				//z = vz0 + (b * vz10) + (c * vz20);
				const __m256 z = _mm256_add_ps(vz0, _mm256_add_ps(
					_mm256_mul_ps(b, vz10), _mm256_mul_ps(c, vz20)));


				//cmp z < depth_buffer
				const __m256 current_depth = *dp;
				const __m256i mask1_8 = _mm256_castps_si256(_mm256_and_ps(
					_mm256_cmp_ps(z, current_depth, _CMP_LT_OQ),
					_mm256_castsi256_ps(mask0_8)));

				if (_mm256_movemask_epi8(mask1_8) != 0)
				{
					//*dp = z
					_mm256_maskstore_ps((float*)dp, mask1_8, z);


					//w = 1.0f / (vw0 + b * vw10 + c * vw20);
					__m256 w = _mm256_fmadd_ps(c, vw20, _mm256_fmadd_ps(b, vw10, vw0));
					//w = _mm256_rcp_ps(w);
					w = _mm256_div_ps(_mm256_set1_ps(1.0f), w);

					//u = w * (a0[0] + b * a10[0] + c * a20[0]);
					__m256 u = _mm256_mul_ps(w,
						_mm256_fmadd_ps(c, a20[0], _mm256_fmadd_ps(b, a10[0], a0[0])));

					//v = w * (a0[1] + b * a10[1] + c * a20[1]);
					__m256 v = _mm256_mul_ps(w,
						_mm256_fmadd_ps(c, a20[1], _mm256_fmadd_ps(b, a10[1], a0[1])));

					int32_t miplevel;
#if ENABLE_MIPMAPPING
					{
						const __m256i ui = _mm256_cvtps_epi32(_mm256_mul_ps(u, _mm256_set1_ps(texture->widthf * 16.0f)));
						const __m256i vi = _mm256_cvtps_epi32(_mm256_mul_ps(v, _mm256_set1_ps(texture->heightf * 16.0f)));

						const __m256i ui0 = _mm256_permutevar8x32_epi32(ui, _mm256_set_epi32(6, 7, 4, 5, 2, 3, 0, 1));
						const __m256i ui1 = _mm256_permutevar8x32_epi32(ui, _mm256_set_epi32(3, 2, 1, 0, 7, 6, 5, 4));
						const __m256i dui0 = _mm256_abs_epi32(_mm256_sub_epi32(ui, ui0));
						const __m256i dui1 = _mm256_abs_epi32(_mm256_sub_epi32(ui, ui1));
						const __m256i dui = _mm256_max_epi32(dui0, dui1);


						const __m256i vi0 = _mm256_permutevar8x32_epi32(vi, _mm256_set_epi32(6, 7, 4, 5, 2, 3, 0, 1));
						const __m256i vi1 = _mm256_permutevar8x32_epi32(vi, _mm256_set_epi32(3, 2, 1, 0, 7, 6, 5, 4));
						const __m256i dvi0 = _mm256_abs_epi32(_mm256_sub_epi32(vi, vi0));
						const __m256i dvi1 = _mm256_abs_epi32(_mm256_sub_epi32(vi, vi1));
						const __m256i dvi = _mm256_max_epi32(dvi0, dvi1);

						const __m256i duvi = _mm256_and_si256(mask1_8, _mm256_max_epi32(dui, dvi));

						union { __m256i max_duv8; uint32_t max_duv[8]; }uni;
						{
							__m256i m0, m1;
							m0 = _mm256_permutevar8x32_epi32(duvi, _mm256_set_epi32(6, 7, 4, 5, 2, 3, 0, 1));
							m1 = _mm256_max_epi32(duvi, m0);
							m0 = _mm256_permutevar8x32_epi32(m1, _mm256_set_epi32(4, 5, 6, 7, 0, 1, 2, 3));
							m0 = _mm256_max_epi32(m1, m0);
							m1 = _mm256_permutevar8x32_epi32(m0, _mm256_set_epi32(0, 1, 2, 3, 4, 5, 6, 7));
							uni.max_duv8 = _mm256_max_epi32(m0, m1);
						}
						uint32_t a = uni.max_duv[0] >> 4;
						miplevel = (int32_t)(30 - _lzcnt_u32(a));
						miplevel = max(0, min(texture->max_mip, miplevel));
					}
#else
					miplevel = 0;
#endif
					const uint8_t* const restrict idx_buffer = texture->mip_ptrs[miplevel];
					const uint32_t tw = texture->width >> miplevel;
					const uint32_t th = texture->height >> miplevel;

					//u = u * (float)texw;
					u = _mm256_mul_ps(u, _mm256_set1_ps(texture->widthf - 1));
					//v = v * (float)texw;
					v = _mm256_mul_ps(v, _mm256_set1_ps(texture->heightf - 1));

					__declspec(align(32)) int32_t tu[8], tv[8];
					__m256i tu8, tv8;
#if DEBUG_SETTINGS & NO_BILINEAR
					tu8 = _mm256_srli_epi32(_mm256_cvtps_epi32(u), miplevel);
					tv8 = _mm256_srli_epi32(_mm256_cvtps_epi32(v), miplevel);
#else
					//http://www.flipcode.com/archives/Texturing_As_In_Unreal.shtml
					tu8 = _mm256_cvtps_epi32(_mm256_mul_ps(u, _mm256_set1_ps(4.0f)));
					tv8 = _mm256_cvtps_epi32(_mm256_mul_ps(v, _mm256_set1_ps(4.0f)));

					const __m256i lut_u[4] = {
						_mm256_set1_epi32(0b01), _mm256_set1_epi32(0b10),
						_mm256_set1_epi32(0b11), _mm256_set1_epi32(0b00) };
					const __m256i lut_v[4] = {
						_mm256_set1_epi32(0b00), _mm256_set1_epi32(0b11),
						_mm256_set1_epi32(0b10), _mm256_set1_epi32(0b01) };

					const __m256i mask_u = _mm256_cmpeq_epi32(_mm256_and_si256(tu8,
						_mm256_set1_epi32(1)), _mm256_setzero_si256());
					const __m256i mask_v = _mm256_cmpeq_epi32(_mm256_and_si256(tv8,
						_mm256_set1_epi32(1)), _mm256_setzero_si256());

					const __m256i u_top = _mm256_or_si256(
						_mm256_and_si256(mask_u, lut_u[0]),
						_mm256_andnot_si256(mask_u, lut_u[1]));
					const __m256i u_bot = _mm256_or_si256(
						_mm256_and_si256(mask_u, lut_u[2]),
						_mm256_andnot_si256(mask_u, lut_u[3]));
					const __m256i offset_u = _mm256_or_si256(
						_mm256_and_si256(mask_v, u_top),
						_mm256_andnot_si256(mask_v, u_bot));
					tu8 = _mm256_srli_epi32(_mm256_add_epi32(tu8, offset_u), 2);

					const __m256i v_top = _mm256_or_si256(
						_mm256_and_si256(mask_u, lut_v[0]),
						_mm256_andnot_si256(mask_u, lut_v[1]));
					const __m256i v_bot = _mm256_or_si256(
						_mm256_and_si256(mask_u, lut_v[2]),
						_mm256_andnot_si256(mask_u, lut_v[3]));
					const __m256i offset_v = _mm256_or_si256(
						_mm256_and_si256(mask_v, v_top),
						_mm256_andnot_si256(mask_v, v_bot));
					tv8 = _mm256_srli_epi32(_mm256_add_epi32(tv8, offset_v), 2);
#endif
					tu8 = _mm256_and_si256(tu8, _mm256_set1_epi32(tw - 1));
					tv8 = _mm256_and_si256(tv8, _mm256_set1_epi32(th - 1));

					//Fetch from texture
					_mm256_store_si256((__m256i*)&tu, tu8);
					_mm256_store_si256((__m256i*)&tv, tv8);
					__declspec(align(32)) uint32_t col[8];
					for (int ci = 0; ci < 8; ++ci)
					{
						const uint32_t mx = _pdep_u32(tu[ci], 0x55555555);
						const uint32_t my = _pdep_u32(tv[ci], 0xAAAAAAAA);
						const uint32_t idx = idx_buffer[mx | my];
						col[ci] = texture->palette[idx];
					}
					const __m256i col8 = _mm256_load_si256((__m256i*)col);
					_mm256_maskstore_epi32((int*)p, mask1_8, col8);
					}
				}
			w0 = _mm256_sub_epi32(w0, fdy01_v8);
			w1 = _mm256_sub_epi32(w1, fdy12_v8);
			w2 = _mm256_sub_epi32(w2, fdy20_v8);
			}
		w0_row_v = _mm256_add_epi32(w0_row_v, fdx01_v2);
		w1_row_v = _mm256_add_epi32(w1_row_v, fdx12_v2);
		w2_row_v = _mm256_add_epi32(w2_row_v, fdx20_v2);
		scanline += TILE_SIZE * 2;
		dscanline += TILE_SIZE * 2;
		}
#endif
	}


static void rasterizeTriJob(void* job_args)
{
	Tile* tile = (Tile*)job_args;
	const uint32_t tri_count = (uint32_t)tile->tri_count;
	for (uint32_t t = 0; t < tri_count; ++t)
	{
		//rasterize the triangle
		rasterizeTriangle(
			tile->tri_buffer[t],
			tile->x,
			tile->y,
			tile->cbuffer,
			tile->dbuffer);
	}
	tile->tri_count = 0;
}

typedef struct JARGS
{
	const float3* verts;
	const float2* texcoords;
	uint32_t vertcount;
	const uint32_t* indices;
	uint32_t indexcount;
	const Texture* texture;
	float4x4 matrix;
}JARGS;

JARGS jargs_buffer[1024 * 1024];
uint32_t jargs_idx = 0;

static void transAndBin(void* ptr)
{
	JARGS* args = (JARGS*)ptr;
	const float3* verts = args->verts;
	const float2* texcoords = args->texcoords;
	uint32_t vertcount = args->vertcount;
	const uint32_t* indices = args->indices;
	uint32_t indexcount = args->indexcount;
	const Texture* texture = args->texture;

	float4* tverts = alloca(vertcount * sizeof(float4));
	{
		const __m128 m[4] = {
			_mm_load_ps(args->matrix[0]),
			_mm_load_ps(args->matrix[1]),
			_mm_load_ps(args->matrix[2]),
			_mm_load_ps(args->matrix[3]) };

		for (uint32_t i = 0; i < vertcount; ++i)
		{
			const __m128 v = _mm_setr_ps(verts[i][0], verts[i][1], verts[i][2], 1.0f);

			//From GLM
			const __m128 vx = _mm_shuffle_ps(v, v, _MM_SHUFFLE(0, 0, 0, 0));
			const __m128 vy = _mm_shuffle_ps(v, v, _MM_SHUFFLE(1, 1, 1, 1));
			const __m128 vz = _mm_shuffle_ps(v, v, _MM_SHUFFLE(2, 2, 2, 2));
			const __m128 vw = _mm_shuffle_ps(v, v, _MM_SHUFFLE(3, 3, 3, 3));


			_mm_store_ps(tverts[i], _mm_fmadd_ps(m[0], vx, _mm_fmadd_ps(m[1], vy, _mm_fmadd_ps(m[2], vz, _mm_mul_ps(m[3], vw)))));

			//const __m128 m0 = _mm_mul_ps(m[0], vx);
			//const __m128 m1 = _mm_mul_ps(m[1], vy);
			//const __m128 m2 = _mm_mul_ps(m[2], vz);
			//const __m128 m3 = _mm_mul_ps(m[3], vw);

			//_mm_store_ps(tverts[i],
			//	_mm_add_ps(_mm_add_ps(m0, m1), _mm_add_ps(m2, m3)));
		}
	}

	//Foreach triangle
	for (uint32_t i = 0; i < indexcount; i += 3)
	{
#define MAX_INDEX (MAX_VERT - 2) * 3
		float fverts0[MAX_VERT * 4], fverts1[MAX_VERT * 4];
		float fattr0[MAX_VERT * ATTR_COUNT], fattr1[MAX_VERT * ATTR_COUNT];
		for (int j = 0; j < 3; ++j)
		{
			const int idx = indices[i + j];
			fverts0[j * 4 + 0] = tverts[idx][0];
			fverts0[j * 4 + 1] = tverts[idx][1];
			fverts0[j * 4 + 2] = tverts[idx][2];
			fverts0[j * 4 + 3] = tverts[idx][3];

			for (int a = 0; a < ATTR_COUNT; ++a)
				fattr0[j * ATTR_COUNT + a] = texcoords[idx][a];
		}

		const int cmask0 = genClipMask(_mm_load_ps(fverts0 + 0));
		const int cmask1 = genClipMask(_mm_load_ps(fverts0 + 4));
		const int cmask2 = genClipMask(_mm_load_ps(fverts0 + 8));
		const int cmask = cmask0 | cmask1 | cmask2;

		//If all vertices are outside one plane, the triangle is guarenteed to
		// be outside of our view, so we can skip it
		if (cmask0 & cmask1 & cmask2) continue;


		int clipped_vert_count = 3;
		float* restrict fverts_in = fverts0, *restrict fverts_out = fverts1;
		float* restrict fattr_in = fattr0, *restrict fattr_out = fattr1;

		if (cmask)
		{
			static const float negf[2] = { -1.0f, 1.0f };
			for (int p = 0; p < 6; ++p)
			{
				if ((cmask & (1 << p)) == 0)
					continue;

				const int axis = p >> 1;
				const float neg = negf[p & 1];

				uint32_t out_count = 0;
				const int idx0 = (clipped_vert_count - 1) * 4;
				const int aidx0 = (clipped_vert_count - 1) * ATTR_COUNT;
				const float* restrict v0 = fverts_in + idx0;
				const float* restrict a0 = fattr_in + aidx0;
				float d0 = v0[axis] * neg + v0[3];

				int was_inside = d0 >= 0;
				for (int j = 0; j < clipped_vert_count; ++j)
				{
					const int idx1 = j * 4;
					const int aidx1 = j * ATTR_COUNT;
					const float* restrict const v1 = fverts_in + idx1;
					const float* restrict const a1 = fattr_in + aidx1;
					const float d1 = v1[axis] * neg + v1[3];

					if (was_inside)
					{
						if (d1 >= 0) //staying in
						{
							fverts_out[out_count * 4 + 0] = v1[0];
							fverts_out[out_count * 4 + 1] = v1[1];
							fverts_out[out_count * 4 + 2] = v1[2];
							fverts_out[out_count * 4 + 3] = v1[3];

							for (int a = 0; a < ATTR_COUNT; ++a)
								fattr_out[out_count * ATTR_COUNT + a] = a1[a];
							out_count++;
						}
						else //going out
						{
							//store interpolated vertex
							const float t = (d0 / (d0 - d1));
							fverts_out[out_count * 4 + 0] = v0[0] + t * (v1[0] - v0[0]);
							fverts_out[out_count * 4 + 1] = v0[1] + t * (v1[1] - v0[1]);
							fverts_out[out_count * 4 + 2] = v0[2] + t * (v1[2] - v0[2]);
							fverts_out[out_count * 4 + 3] = v0[3] + t * (v1[3] - v0[3]);
							//fverts_out[out_count * 4 + axis] = fverts_out[out_count * 4 + 3] * -neg;
							const int attr_idx = out_count * ATTR_COUNT;
							for (int a = 0; a < ATTR_COUNT; ++a)
								fattr_out[attr_idx + a] = a0[a] + t * (a1[a] - a0[a]);

							out_count++;
							was_inside = 0;
						}
					}
					else if (d1 >= 0)  //going in
					{
						//Store interpolated vertex
						const float t = (d0 / (d0 - d1));
						fverts_out[out_count * 4 + 0] = v0[0] + t * (v1[0] - v0[0]);
						fverts_out[out_count * 4 + 1] = v0[1] + t * (v1[1] - v0[1]);
						fverts_out[out_count * 4 + 2] = v0[2] + t * (v1[2] - v0[2]);
						fverts_out[out_count * 4 + 3] = v0[3] + t * (v1[3] - v0[3]);
						//fverts_out[out_count * 4 + axis] = fverts_out[out_count * 4 + 3] * -neg;
						const int attr_idx0 = out_count * ATTR_COUNT;
						for (int a = 0; a < ATTR_COUNT; ++a)
							fattr_out[attr_idx0 + a] = a0[a] + t * (a1[a] - a0[a]);
						out_count++;

						//Store vertex
						fverts_out[out_count * 4 + 0] = v1[0];
						fverts_out[out_count * 4 + 1] = v1[1];
						fverts_out[out_count * 4 + 2] = v1[2];
						fverts_out[out_count * 4 + 3] = v1[3];
						const int attr_idx1 = out_count * ATTR_COUNT;
						for (int a = 0; a < ATTR_COUNT; ++a)
							fattr_out[attr_idx1 + a] = a1[a];
						out_count++;
						was_inside = 1;
					}

					v0 = v1;
					a0 = a1;
					d0 = d1;
				}
				clipped_vert_count = out_count;
				float* t = fverts_in; fverts_in = fverts_out; fverts_out = t;
				t = fattr_in; fattr_in = fattr_out; fattr_out = t;
			}
		}

		const float* fverts = fverts_in;
		const float* fattr = fattr_in;

		int32_t iverts[MAX_VERT][2];
		float vertsz[MAX_VERT];
		float vertsw[MAX_VERT];
		float attrq[MAX_VERT][ATTR_COUNT];

		for (int j = 0; j < clipped_vert_count; ++j)
		{
			const float x = fverts[j * 4 + 0];
			const float y = fverts[j * 4 + 1];
			const float z = fverts[j * 4 + 2];
			const float w = fverts[j * 4 + 3];

			iverts[j][0] = (int32_t)(((x / w) * 0.5f + 0.5f) * (s_render_target->width)* MULF);
			iverts[j][1] = (int32_t)(((y / w) * -.5f + 0.5f) * (s_render_target->height)* MULF);
			vertsw[j] = 1.0f / w;
			vertsz[j] = (z / w) *.5f + .5f;

			for (int a = 0; a < ATTR_COUNT; ++a)
				attrq[j][a] = fattr[j * ATTR_COUNT + a] / w;
		}

		int clipped_index_count = (clipped_vert_count - 2) * 3;

		static const int32_t clipped_indices0[] = { 0,1,2, 0,2,3, 0,3,4, 0,4,5, 0,5,6, 0,6,7, 0,7,8 };
		static const int32_t clipped_indices1[] = { 0,2,1, 0,3,2, 0,4,3, 0,5,4, 0,6,4, 0,7,6, 0,8,7 };
		const int32_t* clipped_indices = clipped_indices0;

		//Backface culling and triangle winding order flipping
		{
			//fetch the x and coordinates
			const int32_t vx[3] = { iverts[0][0], iverts[1][0], iverts[2][0] };
			const int32_t vy[3] = { iverts[0][1], iverts[1][1], iverts[2][1] };

			//do a cross product on the edges of the triangle
			const int32_t ex0 = vx[1] - vx[0];
			const int32_t ey0 = vy[1] - vy[0];
			const int32_t ex1 = vx[2] - vx[0];
			const int32_t ey1 = vy[2] - vy[0];
			const int32_t rz = ex0 * ey1 - ey0 * ex1;

#if BACKFACE_CULLING == 1

			//Apply back face culling
#  if WINDING_ORDER == WINDING_ORDER_CCW
			if (rz > 0xff)
#  elif WINDING_ORDER == WINDING_ORDER_CW
			if (rz < -0xff)
#  endif
				continue;

#else
			//If bacface culling is disabled, we need to swap the winding order around
			// in order for the triangle to be drawn
			if (rz > 0) clipped_indices = clipped_indices1;
#endif
		}

		for (int j = 0; j < clipped_index_count; j += 3)
		{
			const int32_t idx0 = clipped_indices[j + 0];
			const int32_t idx1 = clipped_indices[j + 1];
			const int32_t idx2 = clipped_indices[j + 2];
			Triangle tri = {
				.vx = { iverts[idx0][0], iverts[idx1][0], iverts[idx2][0] },
				.vy = { iverts[idx0][1], iverts[idx1][1], iverts[idx2][1] },
				.vertsw = { vertsw[idx0], vertsw[idx1], vertsw[idx2] },
				.vertsz = { vertsz[idx0], vertsz[idx1], vertsz[idx2] },
				.texture = texture
			};
			for (int a = 0; a < ATTR_COUNT; ++a)
			{
				tri.attr[a * 3 + 0] = attrq[idx0][a];
				tri.attr[a * 3 + 1] = attrq[idx1][a];
				tri.attr[a * 3 + 2] = attrq[idx2][a];
			}

#define TOTAL_SHIFT (SHIFT + TILE_SHIFT)
#define TOTAL_MASK ((1 << TOTAL_SHIFT) - 1)
			uint32_t min_x = min3(tri.vx[0], tri.vx[1], tri.vx[2]);
			uint32_t min_y = min3(tri.vy[0], tri.vy[1], tri.vy[2]);
			uint32_t max_x = max3(tri.vx[0], tri.vx[1], tri.vx[2]);
			uint32_t max_y = max3(tri.vy[0], tri.vy[1], tri.vy[2]);

			uint32_t min_tx = min_x >> TOTAL_SHIFT;                //round down
			uint32_t min_ty = min_y >> TOTAL_SHIFT;                //round down
			uint32_t max_tx = (max_x + TOTAL_MASK) >> TOTAL_SHIFT; //round up
			uint32_t max_ty = (max_y + TOTAL_MASK) >> TOTAL_SHIFT; //round up
#undef TOTAL_MASK
#undef TOTAL_SHIFT

			Tile* tile_line = s_tiles + min_tx + min_ty * s_tile_pitch;
			for (uint32_t ty = min_ty; ty < max_ty; ++ty)
			{
				Tile* tile = tile_line;
				for (uint32_t tx = min_tx; tx < max_tx; ++tx, ++tile)
				{
					const LONG idx = InterlockedIncrement(&tile->tri_count) - 1;
					tile->tri_buffer[idx] = tri;
				}
				tile_line += s_tile_pitch;
			}
		}
	}
}

void drawMesh(
	const float3* verts,
	const float2* texcoords,
	uint32_t vertcount,
	const uint32_t* indices,
	uint32_t indexcount,
	const Texture* texture,
	float4x4 matrix)
{
	JARGS job_args = {
		.verts = verts,
		.texcoords = texcoords,
		.vertcount = vertcount,
		.indices = indices,
		.indexcount = indexcount,
		.texture = texture
	};

	mat4x4_dup(job_args.matrix, matrix);
	jargs_buffer[jargs_idx++] = job_args;
}

void flushRenderer()
{
	//Transforming and binning
	for (uint32_t i = 0; i < jargs_idx; ++i)
	{
		Job job = {
			.function = &transAndBin,
			.param = &jargs_buffer[i]
		};
#if ENABLE_MULTI_THREADING
		JS_queueJob(job_system, &job);
#else
		job.function(job.param);
#endif
	}
	jargs_idx = 0;
#if ENABLE_MULTI_THREADING
	JS_start(job_system);
	JS_sync(job_system);
#endif



	//Rasterization
	for (uint32_t i = 0; i < s_tile_count; ++i)
	{
		Job job = {
			.function = &rasterizeTriJob,
			.param = &s_tiles[i]
		};
#if ENABLE_MULTI_THREADING
		JS_queueJob(job_system, &job);
#else
		job.function(job.param);
#endif
	}
#if ENABLE_MULTI_THREADING
	JS_start(job_system);
	JS_sync(job_system);
#endif
}
