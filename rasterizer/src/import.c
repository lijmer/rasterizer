#include "import.h"
#include "util.h"
#include "palettize.h"

#include "stb_image.h"


#include "tiny_windows.h"
#include <Windows.h>

#include <assimp/cimport.h>
#include <assimp/postprocess.h>
#include <assimp/scene.h>

#include <immintrin.h>

#define MAX_PATH 260

static float srgbToLinear(float c)
{
	return (c <= 0.04045) ?
		(c * (1.0f / 12.92f)) :
		powf((c + 0.055f) / 1.055f, 2.4f);
}
static float linearToSRGB(float c)
{
	return (c <= 0.0031308f) ?
		c * 12.92f :
		(1.055f) * powf(c, 1 / 2.4f) - 0.055f;
}


static uint32_t blendPixels(uint32_t a, uint32_t b, uint32_t c, uint32_t d)
{
	float ra = (float)((a >> 16) & 0xff) * (1.0f / 255.0f);
	float rb = (float)((b >> 16) & 0xff) * (1.0f / 255.0f);
	float rc = (float)((c >> 16) & 0xff) * (1.0f / 255.0f);
	float rd = (float)((d >> 16) & 0xff) * (1.0f / 255.0f);

	float ga = (float)((a >> 8) & 0xff) * (1.0f / 255.0f);
	float gb = (float)((b >> 8) & 0xff) * (1.0f / 255.0f);
	float gc = (float)((c >> 8) & 0xff) * (1.0f / 255.0f);
	float gd = (float)((d >> 8) & 0xff) * (1.0f / 255.0f);


	float ba = (float)(a & 0xff) * (1.0f / 255.0f);
	float bb = (float)(b & 0xff) * (1.0f / 255.0f);
	float bc = (float)(c & 0xff) * (1.0f / 255.0f);
	float bd = (float)(d & 0xff) * (1.0f / 255.0f);


	ra = srgbToLinear(ra);
	rb = srgbToLinear(rb);
	rc = srgbToLinear(rc);
	rd = srgbToLinear(rd);

	ga = srgbToLinear(ga);
	gb = srgbToLinear(gb);
	gc = srgbToLinear(gc);
	gd = srgbToLinear(gd);

	ba = srgbToLinear(ba);
	bb = srgbToLinear(bb);
	bc = srgbToLinear(bc);
	bd = srgbToLinear(bd);

	float result_r = (ra + rb + rc + rd) * .25f;
	float result_g = (ga + gb + gc + gd) * .25f;
	float result_b = (ba + bb + bc + bd) * .25f;

	result_r = linearToSRGB(result_r);
	result_g = linearToSRGB(result_g);
	result_b = linearToSRGB(result_b);

	return
		((uint32_t)(result_r * 255.0f) << 16) |
		((uint32_t)(result_g * 255.0f) << 8) |
		(uint32_t)(result_b * 255.0f);
}

static void calcPixelCountMips(
	size_t w,
	size_t h,
	size_t* pixel_count,
	size_t* mip_count)
{
	*mip_count = 0;
	*pixel_count = 0;
	for (;;)
	{
		*mip_count += 1;
		*pixel_count += w * h;

		if (w == 1 && h == 1)
			break;
		if (w > 1) w >>= 1;
		if (h > 1) h >>= 1;
	}
}

/*
return values:
* -1 -> failed to load from file
* -2 -> texture is not power of two
*/
int32_t importTexture(const char* filepath, Texture* texture)
{
	if (filepath == NULL || texture == NULL)
		return ERR_PARAM_WAS_NULL;

	int w, h, comp;
	stbi_uc* stb_img = stbi_load(filepath, &w, &h, &comp, 0);
	if (stb_img == NULL || comp < 3) return -1;

	if (!isPowerOfTwo(w) || !isPowerOfTwo(h))
	{
		stbi_image_free(stb_img);
		return -2;
	}

	size_t buffer_and_mips_size;
	size_t mip_count;
	calcPixelCountMips(w, h, &buffer_and_mips_size, &mip_count);
	mip_count = min(mip_count, 11);
	uint32_t* color_buffer = MALLOC64(buffer_and_mips_size * sizeof(uint32_t));
	if (color_buffer == NULL)
	{
		stbi_image_free(stb_img);
		return ERR_ALLOC_FAILURE;
	}

	size_t size = w * h;
	for (size_t i = 0; i < size; ++i)
	{
		//swap byte order
		uint32_t r = stb_img[i * comp + 0];
		uint32_t g = stb_img[i * comp + 1];
		uint32_t b = stb_img[i * comp + 2];
		uint32_t a = (comp == 4) ? stb_img[i * comp + 3] : 0xff;
		color_buffer[i] = (a << 24) | (r << 16) | (g << 8) | b;
	}

	//generate mips
	{
		size_t bw = w, bh = h;
		uint32_t* b = color_buffer;
		for (int i = 0; i < mip_count; ++i)
		{
			size_t sw, sh;
			if (bw > 1) sw = bw >> 1; else sw = bw;
			if (bh > 1) sh = bh >> 1; else sh = bh;

			uint32_t* scanline_big = b;
			uint32_t* scanline_small = b + bw * bh;
			for (size_t y = 0; y < sh; ++y)
			{
				for (size_t x = 0; x < sw; ++x)
				{
					scanline_small[x] = blendPixels(
						scanline_big[00 + (x << 1) + 0],
						scanline_big[00 + (x << 1) + 1],
						scanline_big[bw + (x << 1) + 0],
						scanline_big[bw + (x << 1) + 1]);
				}
				scanline_big += bw * 2;
				scanline_small += sw;
			}

			b += bw * bh;
			bw = sw;
			bh = sh;
		}
	}

	uint8_t* idx_buffer = MALLOC64(buffer_and_mips_size);
	uint32_t* palette = MALLOC64(256 * sizeof(uint32_t));

	palettize256(
		color_buffer,
		buffer_and_mips_size,
		idx_buffer,
		palette);


	//generate mip pointers and swizzle to morton order
	uint8_t* morton = MALLOC64(buffer_and_mips_size);
	{
		size_t width = w;
		size_t height = h;
		uint8_t* morton_mip = morton;
		uint8_t* idx_buffer_mip = idx_buffer;
		for (size_t i = 0; i < mip_count; ++i)
		{

			for (size_t y = 0; y < height; ++y)
			{
				const uint32_t my = MORTON32((uint32_t)y) << 1;
				for (size_t x = 0; x < width; ++x)
				{
					const uint32_t mx = MORTON32((uint32_t)x);
					morton_mip[mx | my] = idx_buffer_mip[y * width + x];
				}
			}

			texture->mip_ptrs[i] = morton_mip;

			morton_mip += width * height;
			idx_buffer_mip += width * height;
			width >>= 1;
			height >>= 1;
		}
	}

	for (uint32_t y = 0; y < (uint32_t)h; ++y)
	{
		const uint32_t my = MORTON32(y) << 1;
		for (uint32_t x = 0; x < (uint32_t)w; ++x)
		{
			const uint32_t mx = MORTON32(x);
			morton[mx | my] = idx_buffer[y * w + x];
		}
	}


	texture->buffer = color_buffer;
	texture->idx_buffer = idx_buffer;
	texture->morton = morton;
	texture->palette = palette;
	texture->max_mip = (int32_t)(mip_count - 1);
	texture->width = (uint32_t)w;
	texture->height = (uint32_t)h;
	texture->widthf = (float)w;
	texture->heightf = (float)h;
	texture->pitch = texture->width;
	texture->shift_w = _tzcnt_u32(texture->width);
	texture->shift_h = _tzcnt_u32(texture->height);



	stbi_image_free(stb_img);

	return 0;
}

void freeTexture(Texture* texture)
{
	FREE64(texture->buffer);
	FREE64(texture->idx_buffer);
	FREE64(texture->palette);
	FREE64(texture->morton);
	*texture = (Texture) { 0 };
}

int32_t extractBasePath(char* base_path, int32_t buf_size, const char* filepath)
{
	const int32_t len = (int32_t)strlen(filepath);
	int32_t i;
	for (i = len - 1; i >= 0; --i)
	{
		if (filepath[i] == '/' || filepath[i] == '\\')
			break;
	}
	i++;
	if (i >= buf_size)
		return 1;

	if (i > 0) memcpy(base_path, filepath, i);
	base_path[i] = '\0';

	return 0;
}

int32_t importModel(
	const char* filepath,
	Model* out_model)
{
	char base_path[MAX_PATH];
	base_path[0] = '\0';
	extractBasePath(base_path, sizeof(base_path), filepath);

	const struct aiScene* scene = aiImportFile(
		filepath,
		aiProcess_JoinIdenticalVertices |
		aiProcess_Triangulate |
		aiProcess_FlipUVs |
		aiProcess_OptimizeGraph);
	if (scene == NULL)
	{
		const char* err = aiGetErrorString();
		char buffer[512];
		sprintf_s(
			buffer,
			sizeof(buffer),
			"Assimp failed to import \"%s\". Error: %s\n",
			filepath,
			err);
		OutputDebugStringA(buffer);
	}


	uint32_t mesh_count = scene->mNumMeshes;
	Mesh* meshes = MALLOC64(mesh_count * sizeof(Mesh));
	for (uint32_t m = 0; m < mesh_count; ++m)
	{
		Mesh* mesh = &meshes[m];
		struct aiMesh* ai_mesh = scene->mMeshes[m];

		mesh->vertex_count = ai_mesh->mNumVertices;
		mesh->vertex_buffer = MALLOC64(mesh->vertex_count * sizeof(float3));
		memcpy(
			mesh->vertex_buffer,
			ai_mesh->mVertices,
			mesh->vertex_count * sizeof(float3));


		mesh->texcoord_buffer = MALLOC64(mesh->vertex_count * sizeof(float2));
		for (uint32_t i = 0; i < mesh->vertex_count; ++i)
		{
			mesh->texcoord_buffer[i][0] = ai_mesh->mTextureCoords[0][i].x;
			mesh->texcoord_buffer[i][1] = ai_mesh->mTextureCoords[0][i].y;
		}

		mesh->index_count = ai_mesh->mNumFaces * 3;
		mesh->index_buffer = MALLOC64(mesh->index_count * sizeof(uint32_t));
		for (uint32_t i = 0; i < ai_mesh->mNumFaces; ++i)
		{
			mesh->index_buffer[i * 3 + 0] = ai_mesh->mFaces[i].mIndices[0];
			mesh->index_buffer[i * 3 + 1] = ai_mesh->mFaces[i].mIndices[1];
			mesh->index_buffer[i * 3 + 2] = ai_mesh->mFaces[i].mIndices[2];
		}

		float3 min_v, max_v;
		vec3_dup(min_v, mesh->vertex_buffer[0]);
		vec3_dup(max_v, mesh->vertex_buffer[0]);
		for (uint32_t i = 1; i < mesh->vertex_count; ++i)
		{
			min_v[0] = min(min_v[0], mesh->vertex_buffer[i][0]);
			min_v[1] = min(min_v[1], mesh->vertex_buffer[i][1]);
			min_v[2] = min(min_v[2], mesh->vertex_buffer[i][2]);

			max_v[0] = max(max_v[0], mesh->vertex_buffer[i][0]);
			max_v[1] = max(max_v[1], mesh->vertex_buffer[i][1]);
			max_v[2] = max(max_v[2], mesh->vertex_buffer[i][2]);
		}
		vec3_dup(mesh->min, min_v);
		vec3_dup(mesh->max, max_v);
	}

	uint32_t material_count = scene->mNumMaterials;

	//count texture pass
	uint32_t max_texture_count = 0;
	for (uint32_t m = 0; m < material_count; ++m)
	{
		struct aiMaterial* ai_material = scene->mMaterials[m];
		uint32_t tex_count =
			aiGetMaterialTextureCount(ai_material, aiTextureType_DIFFUSE);
		max_texture_count += tex_count;
	}

	//Load texture pass
	Texture* textures = MALLOC64(max_texture_count * sizeof(Texture));
	char* str_buffer = MALLOC64(max_texture_count * MAX_PATH * sizeof(char));
	char** texture_names = MALLOC64(max_texture_count * sizeof(char*));
	for (uint32_t i = 0; i < max_texture_count; ++i)
		texture_names[i] = str_buffer + i * MAX_PATH;

	uint32_t texture_count = 0; //Reset this, since we will use it as the index
	for (uint32_t m = 0; m < material_count; ++m)
	{
		struct aiMaterial* ai_material = scene->mMaterials[m];
		uint32_t tex_count =
			aiGetMaterialTextureCount(ai_material, aiTextureType_DIFFUSE);

		for (uint32_t t = 0; t < tex_count; ++t)
		{
			struct aiString path;
			aiGetMaterialTexture(
				ai_material,
				aiTextureType_DIFFUSE,
				t,
				&path,
				NULL, NULL, NULL, NULL, NULL, NULL);

			for (uint32_t i = 0; i < texture_count; ++i)
			{
				if (strcmp(path.data, texture_names[i]) == 0)
					goto skip;
			}

			char full_path[MAX_PATH];
			strcpy_s(full_path, sizeof(full_path), base_path);
			strcat_s(full_path, sizeof(full_path), path.data);
			strcpy_s(texture_names[texture_count], sizeof(full_path), path.data);

			int32_t result = importTexture(full_path, &textures[texture_count++]);
			ASSERT(result == 0);
			(void)result;
		skip:;
		}
	}


	Material* materials = MALLOC64(material_count * sizeof(Material));
	for (uint32_t m = 0; m < material_count; ++m)
	{
		Material* material = &materials[m];
		struct aiMaterial* ai_material = scene->mMaterials[m];

		struct aiString name;
		aiGetMaterialString(ai_material, AI_MATKEY_NAME, &name);
		strcpy_s(material->name, MAXLEN, name.data);

		uint32_t tex_count =
			aiGetMaterialTextureCount(ai_material, aiTextureType_DIFFUSE);
		if (tex_count > 0)
		{
			struct aiString path;
			aiGetMaterialTexture(
				ai_material,
				aiTextureType_DIFFUSE,
				0,
				&path,
				NULL, NULL, NULL, NULL, NULL, NULL);

			uint32_t i;
			for (i = 0; i < texture_count; ++i)
			{
				if (strcmp(texture_names[i], path.data) == 0)
					break;
			}

			if (i == texture_count) material->texture = NULL;
			else                    material->texture = &textures[i];
		}
		else
		{
			material->texture = NULL;
		}
	}


	struct aiNode* root = scene->mRootNode;

	//Count the number of objects
	uint32_t object_count = 1;
	{
		struct aiNode* current_node = root;
		struct aiNode* stack[4096];
		int stackidx = 0;

		for (;;)
		{
			if (current_node->mNumChildren > 0)
			{
				object_count += current_node->mNumChildren;
				for (uint32_t i = 1; i < current_node->mNumChildren; ++i)
					stack[stackidx++] = current_node->mChildren[i];
				current_node = current_node->mChildren[0];
			}
			else
			{
				if (stackidx == 0) break;
				current_node = stack[--stackidx];
			}
		}
	}


	Object* objects = MALLOC64(sizeof(Object) * object_count);
	uint32_t obj_idx = 1;
	{
		struct aiNode* current_node = root;
		Object* current_object = &objects[0];
		struct
		{
			struct aiNode* node; Object* object;
		} stack[4096];
		int stackidx = 0;

		for (;;)
		{
			*current_object = (Object) { 0 };

			current_object->child_count = current_node->mNumChildren;
			if (current_object->child_count > 0)
			{
				current_object->children = objects + obj_idx;
				obj_idx += current_object->child_count;
			}
			if (current_node->mNumMeshes > 0)
			{
				current_object->mesh_count = current_node->mNumMeshes;


				current_object->meshes =
					MALLOC64(sizeof(Mesh*) * current_object->mesh_count);
				current_object->materials =
					MALLOC64(sizeof(Material*) * current_object->mesh_count);

				for (uint32_t i = 0; i < current_object->mesh_count; ++i)
				{
					uint32_t mesh_idx = current_node->mMeshes[i];
					current_object->meshes[i] = &meshes[mesh_idx];

					struct aiMesh* ai_mesh = scene->mMeshes[mesh_idx];
					uint32_t material_index = ai_mesh->mMaterialIndex;
					current_object->materials[i] = &materials[material_index];

				}
			}
			mat4x4_dup(
				current_object->world_matrix,
				*(float4x4*)&current_node->mTransformation);


			if (current_node->mNumChildren > 0)
			{
				for (uint32_t i = 1; i < current_node->mNumChildren; ++i)
				{
					stack[stackidx].node = current_node->mChildren[i];
					stack[stackidx].object = &current_object->children[i];
					stackidx++;
				}
				current_node = current_node->mChildren[0];
				current_object = &current_object->children[0];
			}
			else
			{
				if (stackidx == 0) break;
				--stackidx;
				current_node = stack[stackidx].node;
				current_object = stack[stackidx].object;
			}
		}
	}


	FREE64(str_buffer);
	FREE64(texture_names);

	out_model->mesh_count = mesh_count;
	out_model->material_count = material_count;
	out_model->texture_count = texture_count;
	out_model->object_count = object_count;
	out_model->meshes = meshes;
	out_model->materials = materials;
	out_model->textures = textures;
	out_model->objects = objects;

	return 0;
}

void freeModel(Model* model)
{
	for (uint32_t i = 0; i < model->mesh_count; ++i)
	{
		FREE64(model->meshes[i].vertex_buffer);
		FREE64(model->meshes[i].texcoord_buffer);
		FREE64(model->meshes[i].index_buffer);
	}
	for (uint32_t i = 0; i < model->texture_count; ++i)
		freeTexture(model->textures + i);

	for (uint32_t i = 0; i < model->object_count; ++i)
	{
		Object* obj = &model->objects[i];
		if (obj->mesh_count > 0)
		{
			FREE64(obj->meshes);
			FREE64(obj->materials);
		}
	}



	FREE64(model->meshes);
	FREE64(model->materials);
	FREE64(model->textures);
	FREE64(model->objects);
	*model = (Model) { 0 };
}
